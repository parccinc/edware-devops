from xml.etree import ElementTree as ET
try:
    from lxml import etree
    from StringIO import StringIO
except:
    # if you're running 2.7, etree works; but you probably don't have lxml installed, so just ignore this.
    pass


def lines_to_list(s):
    return s.splitlines()


def get_by_xpath(xml_string, xpath):
    """
    :param xml_string:
    :param xpath:  e.g. "repositoryFileDto[path='/public']/id"
    :return:
    """
    try:
        tree = ET.fromstring(xml_string)
        paths = tree.findall(xpath)

    except SyntaxError:  # probably python 2.6; try lxml
        #strip off encoding declaration, parse under 2.6.6 doesn't like it
        sio = StringIO(xml_string.replace('encoding="UTF-8"',''))
        tree = etree.parse(sio)
        paths = tree.xpath(xpath)

    return [node.text for node in paths]


def get_attribute_from_list(list_of_items, attribute):
    return [x.get(attribute) for x in list_of_items]


def split_string(input_string, delimiter=','):
    return input_string.split(delimiter)


def aws_formatted_dict(input_dict):
    return ' '.join("Key='{0}',Value='{1}'".format(k,v) for k, v in input_dict.iteritems())


def merge_dict(dict1, dict2):
    result = dict1.copy()
    result.update(dict2)
    return result


class FilterModule(object):
    def filters(self):
        return {
            'lines_to_list': lines_to_list,
            'get_by_xpath': get_by_xpath,
            'get_attribute_from_list': get_attribute_from_list,
            'split_string': split_string,
            'aws_formatted_dict': aws_formatted_dict,
            'merge_dict': merge_dict
        }



---
# ansible-playbook -i ./infrastructure/inventory/dev -e realm="{{ realm }}" ./infrastructure/add-route53-ec2-instances.yml

- hosts: 127.0.0.1
  gather_facts: false
  pre_tasks:
    - name: validate requirements
      fail: msg="The realm variable is not defined. Please run the command with -e realm=your_name_of_realm."
      when: realm is not defined

- hosts: 127.0.0.1
  connection: local
  gather_facts: false
  name: Get current inventory
  tasks:
    - name: Describe the EC2 instances
      command: aws ec2 describe-instances \
          --filter "Name=tag:realm,Values={{ realm }}" "Name=instance-state-name,Values=pending,running" \
          --query 'Reservations[*].Instances[*].Tags[?Key==`type`].Value' --output=text
      register: ec2_description
    - name: Create the host group
      add_host: name={{ item }} groups=current-inventory
      with_items: "{{ ec2_description.stdout | lines_to_list }}"

# Add route53 records for existing ec2 instances
# Will fail if the record already exists
# Serial 1 and pause 1 second for AWS ratelimiting issues
- hosts: current-inventory 
  connection: local
  serial: 1
  tasks:
    - name: Get private IP address
      command: aws ec2 describe-instances \
          --filter "Name=tag:realm,Values={{ realm }}" "Name=tag:type,Values={{inventory_hostname}}" "Name=instance-state-name,Values=pending,running" \
          --query 'Reservations[*].Instances[*].PrivateIpAddress' --output=text
      register: ec2_private_ip
    - name: Add A record to route53 private zone
      route53:
        command: "create"
        zone: "{{ route_53_private_zone }}."
        record: "{{realm}}-{{inventory_hostname}}.{{ route_53_private_zone }}."
        type: A
        ttl: "{{ dns_ttl }}"
        value: "{{ ec2_private_ip.stdout_lines[0] }}"
    - name: Get public IP address
      command: aws ec2 describe-instances \
          --filter "Name=tag:realm,Values={{ realm }}" "Name=tag:type,Values={{inventory_hostname}}" "Name=instance-state-name,Values=pending,running" \
          --query 'Reservations[*].Instances[*].PublicIpAddress' --output=text
      register: ec2_public_ip
    - name: Add A record to route53 public zone
      route53:
        command: "create"
        zone: "{{ route_53_zone }}."
        record: "{{realm}}-{{inventory_hostname}}.{{ route_53_zone }}."
        type: A
        ttl: "{{ dns_ttl }}"
        value: "{{ ec2_public_ip.stdout_lines[0] }}"
      when: ec2_public_ip.stdout_lines and env_var != 'dev' and env_var != 'qa'
    - name: Pause for AWS route53 ratelimiting
      pause: seconds=1

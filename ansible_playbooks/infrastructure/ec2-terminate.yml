---
- hosts: all:&{{ realm }}
  connection: local
  gather_facts: False
  tasks:
    - name: Get security group IDs
      command: aws ec2 describe-security-groups \
          --filters Name=group-name,Values='{{ elb_access_group }}' \
          --query "SecurityGroups[0].GroupId" --output=text
      register: proxy_outbound_access_sg
      when: terminate_elbs
      run_once: true

# Delete the Route53 DNS records
# Serial 1 and pause 1s to bypass route53 ratelimiting issues
- hosts: all:!rds:&{{realm}}
  connection: local
  serial: 1
  tasks:
    - name: Gather route53 private zone data
      route53:
        command: get
        private_zone: True
        zone: "{{ route_53_private_zone }}."
        record: "{{ realm }}-{{ item }}.{{ route_53_private_zone }}."
        type: A
      register: private_dns_record
      with_items: "{{ ec2_tag_type }}"

    - name: Gather route53 public zone data
      route53:
        command: get
        zone: "{{ route_53_zone }}."
        record: "{{ realm }}-{{ item }}.{{ route_53_zone }}."
        type: A
      register: public_dns_record
      with_items: "{{ ec2_tag_type }}"

    - name: Pause for AWS route53 ratelimiting
      pause: seconds=1

    - name: Delete route53 private dns entries
      route53:
        command: delete
        private_zone: True
        zone: "{{ route_53_private_zone }}."
        record: "{{ private_dns_record.results[0].set.record }}"
        ttl: "{{ private_dns_record.results[0].set.ttl }}"
        type: "{{ private_dns_record.results[0].set.type }}"
        value: "{{ private_dns_record.results[0].set.value }}"
      when: private_dns_record.results[0].set is defined and private_dns_record.results[0].set

    - name: Delete route53 public dns entries
      route53:
        command: delete
        zone: "{{ route_53_zone }}."
        record: "{{ public_dns_record.results[0].set.record }}"
        ttl: "{{ public_dns_record.results[0].set.ttl }}"
        type: "{{ public_dns_record.results[0].set.type }}"
        value: "{{ public_dns_record.results[0].set.value }}"
      when: public_dns_record.results[0].set is defined and public_dns_record.results[0].set

    - name: Pause for AWS route53 ratelimiting
      pause: seconds=1

- hosts: utility-cluster:&{{realm}}
  connection: local
  serial: 1
  vars:
    record_action: delete
    record_value: "{{ ec2_private_ip_address }}"
    record_set_id: "{{ realm }}-{{ ec2_tag_type }}"
  roles:
    - route53_wrr

- hosts: utility-cluster:ansible-servers:&{{ realm }}
  connection: local
  gather_facts: False
  serial: 1
  vars:
    transient_proxy_security_group: '{{ proxy_outbound_access_sg.stdout }}'
  tasks:
   - name: Remove port 80 rule of instance from security group rules that allow elb healthchecks
     command: aws ec2 revoke-security-group-ingress \
          --group-id "{{ transient_proxy_security_group }}" \
          --protocol tcp --port 80 --cidr "{{ item }}/32"
     with_items: "{{ ec2_ip_address }}"
     when: terminate_elbs and transient_proxy_security_group != 'None'
     ignore_errors: True

   - name: Remove port 443 rule of instance from security group rules that allow elb healthchecks
     command: aws ec2 revoke-security-group-ingress \
          --group-id "{{ transient_proxy_security_group }}" \
          --protocol tcp --port 443 --cidr "{{ item }}/32"
     with_items: "{{ ec2_ip_address }}"
     when: terminate_elbs and transient_proxy_security_group != 'None'
     ignore_errors: True

- hosts: all:!rds:&{{ realm }}
  connection: local
  tasks:
    - name: Destroy all instances
      local_action:
        module: ec2
        state: 'absent'
        region: '{{ region }}'
        instance_ids: '{{ item }}'
      with_items: "{{ ec2_id }}"

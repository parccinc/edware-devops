#! /bin/bash

set -e

# First get the setup script for Setuptools and virtualenv:
wget https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py
wget https://pypi.python.org/packages/source/v/virtualenv/virtualenv-1.11.6.tar.gz
tar -xf virtualenv-1.11.6.tar.gz
 
# Then install it for Python 2.7 or Python 3.3:
if [ "$1" = "2.7" ]; then
	python2.7 ez_setup.py
	easy_install-2.7 pip
	cd virtualenv-1.11.6
	python2.7 setup.py install
elif [ "$1" == "3.3" ]; then
	python3.3 ez_setup.py
	easy_install-3.3 pip
	cd virtualenv-1.11.6
	python3.3 setup.py install
else
	echo "Incorrect python version"
	exit 1
fi

cd ..
rm ez_setup.py
rm -R virtualenv-1.11.6*
exit 0

#!/usr/bin/env python
from __future__ import print_function

import os
import subprocess
import sys
import tempfile
import time
import signal
from ctypes import cdll
import atexit
import traceback
import argparse

PR_SET_PDEATHSIG = 1
PLAYBOOK_DIRECTORY = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))


class PrCtlError(Exception):
    pass


def on_parent_exit(signame):
    """
    Return a function to be run in a child process which will trigger
    SIGNAME to be sent when the parent process dies

    Used to terminate a child process if the parent dies.

    Note: only works on Linux
    """
    signum = getattr(signal, signame)

    def set_parent_exit_signal():
        try:
            libc = cdll['libc.so.6']
            result = libc.prctl(PR_SET_PDEATHSIG, signum)
            if result != 0:
                raise PrCtlError('prctl failed with error code %s' % result)
        except OSError:
            # probably not a linux box, just return
            pass
    return set_parent_exit_signal


def get_child_pids():
    pid = os.getpid()
    try:
        p = subprocess.Popen(
            [
                '/usr/bin/pgrep',
                '-P',
                str(pid)
            ],
            stderr=subprocess.STDOUT,
            stdout=subprocess.PIPE,
            bufsize=0)
        out, _ = p.communicate()
        return map(int, out.split())

    except Exception as e:
        print('Error getting child PIDs: %s' % (e,))
        print(traceback.format_exc())
        return []


def create_signal_handler(playbook_processes, existing_handler=None):
    """
    Create a handler for a signal, which terminates any running subprocesses and calls any pre-existing handler

    :param existing_handler:
    :return:
    """
    def terminate_children_on_signal(signum, frame):
        print('Received signal %s, cleaing up before exit...' % (signum,))

        signal.signal(signum, existing_handler)  # re-register original signal

        for playbook, process, output_file in playbook_processes:
            print('Terminating %s...' % (playbook.name,))
            process.terminate()  # should work fine if process has already stopped or been terminated
            process.wait()
            output_file.seek(0)
            print('Output from the %s playbook:\n' % playbook.name, output_file.read())
            output_file.close()
            sys.stdout.flush()

        for cpid in get_child_pids():
            print('Terminating child w/ pid %s' % (cpid,))
            os.kill(cpid, signal.SIGTERM)
            os.waitpid(cpid, 0)

        print('Exiting...')
        sys.stdout.flush()
        sys.exit(1)

    return terminate_children_on_signal


def terminate_at_exit(process):
    try:
        process.terminate()
    except OSError:
        pass


class Playbook(object):
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "<Playbook name:%s>" % (self.name)

    def __str__(self):
        return self.name

PLAYBOOKS = [
    Playbook('ec2'),
    Playbook('rds'),
    Playbook('elb'),
    Playbook('efs'),
    Playbook('elasticache'),
]


class PlaybookRunner(object):
    def __init__(self, playbook_additional_args):
        self.playbook_additional_args = playbook_additional_args

    def run_playbook(self, playbook):
        print('Running the %s playbook...' % playbook.name)
        sys.stdout.flush()

        ansible_log_prefix = 'ansible-playbook-%s-' % playbook.name
        output_file = tempfile.NamedTemporaryFile(suffix='.log', prefix=ansible_log_prefix)

        command = [
            'ansible-playbook',
            '-i',
            'infrastructure/inventory/%s' % sys.argv[2],
            '-e',
            'realm=%s' % sys.argv[1],
            'infrastructure/%s.yml' % playbook.name,
        ]
        process = subprocess.Popen(
            command + self.playbook_additional_args,
            cwd=PLAYBOOK_DIRECTORY,
            stderr=subprocess.STDOUT,
            stdout=output_file,
            bufsize=-1,
            preexec_fn=on_parent_exit('SIGTERM'),
        )
        atexit.register(terminate_at_exit, process)  # terminate the subprocess if this process exits normally

        return (playbook, process, output_file)


def main(args, additional_playbook_arguments):
    playbook_runner = PlaybookRunner(additional_playbook_arguments)

    if args.only_run:
        resolved_playbooks = [
            playbook for playbook in PLAYBOOKS if playbook.name in args.only_run
        ]

    elif args.dont_run:
        resolved_playbooks = [
            playbook for playbook in PLAYBOOKS if playbook.name not in args.dont_run
        ]

    else:
        resolved_playbooks = PLAYBOOKS[:]

    playbook_status = 0
    failed_playbooks = []

    playbook_processes = []

    # Register a signal handler for various sorts of interruptions
    for signame in ('SIGTERM', 'SIGINT', 'SIGQUIT'):
        if hasattr(signal, signame):
            signum = getattr(signal, signame)
            existing_handler = signal.getsignal(signum)
            signal.signal(signum, create_signal_handler(playbook_processes, existing_handler))

    # There's a race condition here between when the subprocesses start and when we add them to this collection.
    # We resolve this by having the signal handler kill all subprocesses by PID after processing this collection.
    playbook_processes.extend(map(playbook_runner.run_playbook, resolved_playbooks))

    while len(playbook_processes) > 0:
        # Poll processes until we find one that has finished.
        print('Polling playbook processes...')
        sys.stdout.flush()
        while True:
            index = -1
            for i, (_, process, _) in enumerate(playbook_processes):
                if process.poll() is not None:
                    index = i
                    break
            if index != -1:
                (playbook, process, output_file) = playbook_processes.pop(index)
                break
            else:
                time.sleep(1)

        output_file.seek(0)
        print('Output from the %s playbook:\n' % playbook.name, output_file.read())
        print('%s playbook finished' % (playbook.name,))
        output_file.close()
        sys.stdout.flush()

        if process.returncode != 0:
            failed_playbooks.append(playbook.name)
            playbook_status = process.returncode

    if playbook_status != 0:
        print('FAILURE: the following playbooks failed: %s'
              % (', '.join(str(x) for x in failed_playbooks)), file=sys.stderr)
        sys.exit(playbook_status)

    else:
        print('SUCCESS!')


def parse_known_args(argv=None, namespace=None):
    """
    parse a couple arguments. Anything unknown will be passed as arguments to ansible-playbook
    :param argv:
    :param namespace:
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('realm')
    parser.add_argument('environment')
    parser.add_argument('--only-run', nargs='+', choices=[playbook.name for playbook in PLAYBOOKS], default=[])
    parser.add_argument('--dont-run', nargs='+', choices=[playbook.name for playbook in PLAYBOOKS], default=[])
    return parser.parse_known_args(argv, namespace)


if __name__ == "__main__":
    args, additional_playbook_arguments = parse_known_args()
    main(args, additional_playbook_arguments)

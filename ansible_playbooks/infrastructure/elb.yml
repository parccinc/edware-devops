---
- hosts: 127.0.0.1
  gather_facts: False
  pre_tasks:
    - name: validate requirements
      fail: msg="The realm variable is not defined. Please run the command with -e realm=your_name_of_realm."
      when: realm is not defined

- hosts: elbs
  connection: local
  gather_facts: False
  tasks:
    - name: Get primary ELB Access security group ID
      command: aws ec2 describe-security-groups \
          --filters Name=group-name,Values='{{ item }}' \
          --query "SecurityGroups[0].GroupId" --output=text
      register: primary_elb_access_sg
      when: make_elbs
      with_items: "{{ elb_groups }}"
      run_once: true

- hosts: elbs:!kibana-elb:!pentaho-elb:!sensu-elb
  connection: local
  gather_facts: False
  vars:
    security_groups: "{{primary_elb_access_sg.results | get_attribute_from_list(attribute='stdout')}}"
  tasks:
    - name: Launch webserver ELB
      local_action:
        module: ec2_elb_lb
        name: '{{realm}}-{{inventory_hostname}}'
        state: present
        region: '{{region}}'
        security_group_ids: '{{security_groups}}'
        subnets:
          - '{{subnet_id}}'
        listeners:
          - protocol: http
            load_balancer_port: 80
            instance_port: 80
            instance_protocol: http
          - protocol: https
            load_balancer_port: 443
            instance_protocol: http
            instance_port: 443
            ssl_certificate_id: '{{elb_ssl_iam_path}}'
        health_check:
          ping_protocol: http
          ping_port: 443
          ping_path: /services/heartbeat
          response_timeout: 5 # seconds
          interval: 30 # seconds
          unhealthy_threshold: 2
          healthy_threshold: 3
      when: make_elbs

- hosts: kibana-elb
  connection: local
  gather_facts: False
  vars:
    security_groups: "{{primary_elb_access_sg.results | get_attribute_from_list(attribute='stdout')}}"
  tasks:
    - name: Pausing, trying to avoid AWS throttling
      pause: seconds=2

    - name: Launch webserver ELB
      local_action:
        module: ec2_elb_lb
        name: '{{realm}}-{{inventory_hostname}}'
        state: present
        region: '{{region}}'
        security_group_ids: '{{security_groups}}'
        subnets:
          - '{{subnet_id}}'
        listeners:
          - protocol: https
            load_balancer_port: 443
            instance_port: 80
            instance_protocol: http
            ssl_certificate_id: '{{elb_ssl_iam_path}}'
        health_check:
          ping_protocol: http
          ping_port: 80
          ping_path: /
          response_timeout: 5 # seconds
          interval: 30 # seconds
          unhealthy_threshold: 2
          healthy_threshold: 3
      when: make_elbs

- hosts: pentaho-elb
  connection: local
  gather_facts: False
  vars:
    security_groups: "{{primary_elb_access_sg.results | get_attribute_from_list(attribute='stdout')}}"
  tasks:
    - name: Pausing, trying to avoid AWS throttling
      pause: seconds=2

    - name: Launch webserver ELB
      local_action:
        module: ec2_elb_lb
        name: '{{realm}}-{{inventory_hostname}}'
        state: present
        region: '{{region}}'
        security_group_ids: '{{security_groups}}'
        subnets:
          - '{{subnet_id}}'
        listeners:
          - protocol: http
            load_balancer_port: 80
            instance_port: 80
            instance_protocol: http
          - protocol: https
            load_balancer_port: 443
            instance_protocol: http
            instance_port: 443
            ssl_certificate_id: '{{elb_ssl_iam_path}}'
        health_check:
          ping_protocol: http
          ping_port: 443
          ping_path: /pentaho/ping/alive.gif
          response_timeout: 5 # seconds
          interval: 30 # seconds
          unhealthy_threshold: 2
          healthy_threshold: 3
        stickiness:
          type: application
          enabled: yes
          cookie: JSESSIONID
        idle_timeout: 300
      when: make_elbs

- hosts: sensu-elb
  connection: local
  gather_facts: False
  vars:
    security_groups: "{{primary_elb_access_sg.results | get_attribute_from_list(attribute='stdout')}}"
  tasks:
    - name: Pausing, trying to avoid AWS throttling
      pause: seconds=2

    - name: Launch sensu ELB
      local_action:
        module: ec2_elb_lb
        name: '{{realm}}-{{inventory_hostname}}'
        state: present
        region: '{{region}}'
        security_group_ids: '{{security_groups}}'
        subnets:
          - '{{subnet_id}}'
        listeners:
          - protocol: https
            load_balancer_port: 8443
            instance_port: 8080
            ssl_certificate_id: '{{elb_ssl_iam_path}}'
            instance_protocol: http
          - protocol: https
            load_balancer_port: 443
            instance_protocol: http
            instance_port: 80
            ssl_certificate_id: '{{elb_ssl_iam_path}}'
        health_check:
          ping_protocol: http
          ping_port: 80
          ping_path: /
          response_timeout: 5 # seconds
          interval: 30 # seconds
          unhealthy_threshold: 2
          healthy_threshold: 3
      when: make_elbs

# Create the Route53 DNS records
# Serial 1 and pause 1s to bypass route53 ratelimiting issues
- hosts: elbs
  connection: local
  gather_facts: False
  serial: 1
  tasks:
    - name: Tag ELBs
      local_action: shell aws elb add-tags --load-balancer-name "{{realm}}-{{inventory_hostname}}" --tags "Key=realm,Value={{realm}}" "Key=host_group,Value={{inventory_hostname}}"
      when: make_elbs
    - name: Get the load balancer and policy names
      set_fact:
        elb_name: '{{realm}}-{{inventory_hostname}}'
      when: make_elbs
    - include: tasks/register_elb_dns_name.yml
      when: make_elbs
    - name: Create the CNAME record
      route53:
        command: create
        zone: '{{route_53_zone}}'
        record: '{{realm}}-{{inventory_hostname}}.{{route_53_zone}}'
        type: CNAME
        ttl: 300
        value: '{{elb_dns_name.stdout}}'
        overwrite: yes
      when: make_elbs
    - name: Create custom CNAME records
      route53:
        command: create
        zone: '{{route_53_zone}}'
        record: '{{ elb_custom_hostnames[inventory_hostname] }}.{{route_53_zone}}'
        type: CNAME
        ttl: 300
        value: '{{elb_dns_name.stdout}}'
        overwrite: yes
      when: make_elbs and elb_custom_hostnames[inventory_hostname] is defined
    - name: Pause for AWS route53 ratelimiting
      pause: seconds=2
      when: make_elbs

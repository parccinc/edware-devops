---

firewall_allowed_applications:
  - { port: 80,   proto: tcp } # httpd
  - { port: 8080, proto: tcp } # graphite

shared_buffers: 128MB
work_mem: 1MB
maintenance_work_mem: 16MB

postgresql_configuration: "{{ postgresql_common_configuration }}"

sensu_install_server: true
sensu_rubygems:
  - redphone
  - redis
  - logstash-logger

sensu_dns: '{{orchestrated_env_realm}}-sensu.{{awsacct_env_var}}.ae1.parcdc.net'
sensu_elb_url: "{{ realm }}-sensu-elb{{ hosted_zone }}"

uchiwa_root_url: 'http://{{sensu_dns}}/uchiwa/'
uchiwa_root_url_elb: 'https://{{sensu_elb_url}}/uchiwa/'

grafana_domain: '{{sensu_dns}}'
grafana_domain_elb: '{{sensu_elb_url}}'
grafana_root_url: 'http://{{grafana_domain}}/grafana/'
grafana_root_url_elb: 'https://{{grafana_domain_elb}}/grafana/'

kibana_root_url: 'http://{{orchestrated_env_realm}}-loghost.{{awsacct_env_var}}.ae1.parcdc.net'
kibana_root_url_elb: 'https://{{realm}}-kibana-elb.{{awsacct_env_var}}.parccresults.org'

graphite_allowed_hosts: '*'

sensu_monitoring_topics:
  - postgres
  - memcached
  - redis

monitoring_topics: '{{sensu_monitoring_topics + e2e_monitoring_topics}}'

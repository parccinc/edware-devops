env: aws_stg
awsacct_env_var: 'stg'
release: 'pentaho'
base_dns: "stg.ae1.parcdc.net"
ansible_ssh_user: "root"
orchestrated_env_realm: "{{ orchestrator_realm|default(realm) ~ orchestrated_realm_suffix|default('') }}"

s3_google_authenticator_bucket_name: "amplify-parcc-devops-utils-stg"
tenant_state_codes:
  - tenant: ww
    state_code: WW
  - tenant: xx
    state_code: XX

staging_databases:
  - db: ww
    login_user: '{{staging_db_user}}'
    login_password: '{{vaulted_staging_db_password}}'
  - db: xx
    login_user: '{{staging_db_user}}'
    login_password: '{{vaulted_staging_db_password}}'

#
#Vault Files Configuration
#
vault_filename: "{{orchestrated_env_realm}}_passwords.yml"
vaultkey_filename: "{{orchestrated_env_realm}}_vault_pass.txt"
vaulted_var_file: "../vault_vars/{{vault_filename}}"

#
#AWS Environment Configuration
#
aws_region: 'us-east-1'
aws_zone: 'us-east-1b'
public_CIDR: '10.175.8.0/22'
private_CIDR: '10.175.4.0/22'
rds_CIDR: '10.175.3.0/24'
lz_CIDR: '10.175.12.0/22'

#
#IdP Server Connection Settings
#
idp_host_baseurl: 'https://stg-sso-parcc.btsauce.com/simplesaml/saml2/idp'
idp_entity_id: '{{idp_host_baseurl}}/SSOService.php'

idp_name_qualifier: '{{idp_host_baseurl}}/SSOService.php'
idp_login_url: '{{idp_host_baseurl}}/SSOService.php'
idp_logout_url: '{{idp_host_baseurl}}/SingleLogoutService.php'

pentaho_sp_server_uri: '{{idp_host_baseurl}}/metadata.php'
pentaho_sp_metadata_uri: '{{idp_host_baseurl}}/metadata.php'
pentaho_sp_logout_uri: '{{idp_logout_url}}'

#
#Server CNAME Addresses
#
reporting_server_address: '{{realm}}-reporting-elb{{hosted_zone}}'
hpz_server_address: '{{realm}}-hpz-elb{{hosted_zone}}'
pentaho_server_address: '{{realm}}-pentaho-elb{{hosted_zone}}'

#
#DNS Server Configuration
#
private_dns_bind_server_ip: '10.175.4.10'
default_aws_vpc_dns_provider_ip: '10.175.0.2'
bind_master_pvtip: '10.175.4.10'
aws_vpc_dnsip: '10.175.0.2'

#
#AWS S3 Configuration for Orchestration Server
#
s3_keys_bucket_name: 'amplify-parcc-keys-{{awsacct_env_var}}'
ansible_user_key_file_name: 'amplify_parcc_{{awsacct_env_var}}_ci_adminkeys.pem'
pemkey_filename: '{{ansible_user_key_file_name}}'
ansible_user_key_s3_path: 'ansible-user-key/{{pemkey_filename}}'

#
#YUM Repo Configuration
#
yum_version: 'v1.14'
s3yum_repo_name: '{{s3yum_bucket_basename}}-{{awsacct_env_var}}'
env_var: 'stg'

#
#Application Servers Configuration
#
queue_user: edware
queue_password: '{{vaulted_queue_user_password}}'
dwcluster_db_user: edware
dwcluster_db_password: '{{vaulted_master_db_password}}'
master_db_user: edware
master_db_password: '{{vaulted_master_db_password}}'
staging_db_user: edware
staging_db_password: '{{vaulted_staging_db_password}}'
loader_db_user: edware
loader_db_password: '{{vaulted_loader_db_password}}'
cds_db_user: edware
cds_db_password: '{{vaulted_cds_db_password}}'
edware_stats_user: edware
edware_stats_password: '{{vaulted_stats_db_password}}'
udl2_db_user: udl2
udl2_db_password: '{{vaulted_udl2_db_password}}'
migrate_celery_password: '{{vaulted_migrate_celery_password}}'
starmigrate_celery_password: '{{vaulted_migrate_celery_password}}'
extract_celery_user: edware
extract_celery_password: '{{vaulted_migrate_celery_password}}'
cds_celery_password: '{{vaulted_migrate_celery_password}}'
pdf_services_celery_user: edware
pdf_services_celery_password: '{{vaulted_migrate_celery_password}}'
gpg_passphrase: '{{vaulted_gpg_passphrase}}'
reporting_db_user: edware
reporting_db_password: '{{vaulted_master_db_password}}'
hpz_db_user: hpz
hpz_db_password: '{{vaulted_hpz_db_password}}'
sensu_rabbit_password: '{{vaulted_sensu_rabbit_password}}'
rds_master_password: '{{vaulted_rds_master_password}}'

#
#PGPool Configuration
#
pgpool_debug_level: 0
pgpool_connection_cache: on
pgpool_user_password: 'pool_passwd'

pgpool_healthcheck_db_user: 'pgpool'
pgpool_healthcheck_db_pass: '{{vaulted_pgpool_healthcheck_db_pass}}'
pgpool_healthcheck_db_md5pass: '{{vaulted_pgpool_healthcheck_db_md5pass}}'
pcp_user: postgres
pcp_password: '{{vaulted_pcp_password}}'
pcp_md5_password: '{{vaulted_pcp_md5_password}}'

#
#Landing Zone Configuration
#
lz_server_group: 'udl-lz'
udl_to_lz_user: 'udl_lz_user'
lz_allowed_rsync_hosts: 10.175.4.0/22, 10.175.8.0/22
sftp_group: edwaredataadmin

#
#Database Common Configuration
#
staging_db:
   shared_buff_space: '8GB'
   work_mem: '100MB'
   maintenance_work_mem: '4GB'
dw_db:
   shared_buff_space: '16GB'
   work_mem: '100MB'
   maintenance_work_mem: '8GB'
cds_db:
   shared_buff_space: '16GB'
   work_mem: '100MB'
   maintenance_work_mem: '8GB'
analytics_db:
   shared_buff_space: '16GB'
   work_mem: '100MB'
   maintenance_work_mem: '8GB'

###
#Tenant Specific Configuration
###

#
#Landing Zone Users
#
lz_users_tenants:
  - { user: data_admin_tenant1, tenant: ww, key_name: id_rsa.pub }
  - { user: data_admin_tenant2, tenant: xx, key_name: id_rsa.pub }

#
# UDL Server Configuration
#
udl_server_conf_lines:
    - { regexp: '^celery.backend\s*=', line: "celery.backend = amqp://guest@{{udl_queue_ip}}//"}
    - { regexp: '^celery.broker\s*=', line: "celery.broker = amqp://guest@{{udl_queue_ip}}//"}
    - { regexp: '^edware_stats.db.url\s*=', line: "edware_stats.db.url = postgresql://{{edware_stats_user}}:{{edware_stats_password}}@{{edware_stats_db_host}}:5432/edware_stats"}

    - { regexp: '^prod_db_conn.ww.url\s*=', line: "prod_db_conn.ww.url = postgresql://{{master_db_user}}:{{master_db_password}}@{{master_private_ips['ww']}}:5432/edware"}
    - { regexp: '^target_db_conn.ww.url\s*=', line: "target_db_conn.ww.url = postgresql://{{staging_db_user}}:{{staging_db_password}}@{{udl_staging_db_host}}:5432/ww"}
    - { regexp: '^prod_db_conn.ww.db_schema\s*=', line: "prod_db_conn.ww.db_schema = {{prod_db_schemas['ww']}}"}

    - { regexp: '^prod_db_conn.xx.url\s*=', line: "prod_db_conn.xx.url = postgresql://{{master_db_user}}:{{master_db_password}}@{{master_private_ips['xx']}}:5432/edware"}
    - { regexp: '^target_db_conn.xx.url\s*=', line: "target_db_conn.xx.url = postgresql://{{staging_db_user}}:{{staging_db_password}}@{{udl_staging_db_host}}:5432/xx"}
    - { regexp: '^prod_db_conn.xx.db_schema\s*=', line: "prod_db_conn.xx.db_schema = {{prod_db_schemas['xx']}}"}

    - { regexp: '^udl2_db_conn.url\s*=', line: "udl2_db_conn.url = postgresql://{{udl2_db_user}}:{{udl2_db_password}}@{{loader_db_ip}}:5432/udl2"}

    - { regexp: '^passphrase\s*=', line: "passphrase = {{gpg_passphrase | default('sbac udl2')}}"}

    - { regexp: '^zones.history\s*=', line: "zones.history = {{udl_backup_bucket}}"}

#
#Master Database IPs
#
ww_master_db_ip: "{{ (groups['dw-master-db'] | intersect( groups[realm] ) | intersect(groups['tag_tenant_ww']) | list )[0]}}"
ww_master_private_ip: "{{ hostvars[ww_master_db_ip]['ansible_eth0']['ipv4']['address'] }}"

xx_master_db_ip: "{{ (groups['dw-master-db'] | intersect( groups[realm] ) | intersect(groups['tag_tenant_xx']) | list )[0]}}"
xx_master_private_ip: "{{ hostvars[xx_master_db_ip]['ansible_eth0']['ipv4']['address'] }}"


master_db_ips:
  ww: '{{ww_master_db_ip}}'
  xx: '{{xx_master_db_ip}}'

master_private_ips:
  ww: '{{ww_master_private_ip}}'
  xx: '{{xx_master_private_ip}}'

#
# CDS Database IPs
#
parcc_cds_db_ip: "{{ (groups['cds-master-db-parcc'] | intersect( groups[realm] ) | list )[0]}}"
parcc_cds_private_ip: "{{ hostvars[parcc_cds_db_ip]['ansible_eth0']['ipv4']['address'] }}"

ww_cds_db_ip: "{{ (groups['cds-master-db'] | intersect( groups[realm] ) | intersect(groups['tag_tenant_ww']) | list )[0]}}"
ww_cds_private_ip: "{{ hostvars[ww_cds_db_ip]['ansible_eth0']['ipv4']['address'] }}"

xx_cds_db_ip: "{{ (groups['cds-master-db'] | intersect( groups[realm] ) | intersect(groups['tag_tenant_xx']) | list )[0]}}"
xx_cds_private_ip: "{{ hostvars[xx_cds_db_ip]['ansible_eth0']['ipv4']['address'] }}"

cds_db_ips:
  parcc: '{{parcc_cds_db_ip}}'
  ww: '{{ww_cds_db_ip}}'
  xx: '{{xx_cds_db_ip}}'

cds_private_ips:
  parcc: '{{parcc_cds_private_ip}}'
  ww: '{{ww_cds_private_ip}}'
  xx: '{{xx_cds_private_ip}}'


#
#Analytics Master Database IPs
#
ww_analytics_master_db_ip: "{{ (groups['analytics-master-db'] | intersect( groups[realm] ) | intersect(groups['tag_tenant_ww']) | list )[0]}}"
ww_analytics_master_private_ip: "{{ hostvars[ww_analytics_master_db_ip]['ansible_eth0']['ipv4']['address'] }}"

xx_analytics_master_db_ip: "{{ (groups['analytics-master-db'] | intersect( groups[realm] ) | intersect(groups['tag_tenant_xx']) | list )[0]}}"
xx_analytics_master_private_ip: "{{ hostvars[xx_analytics_master_db_ip]['ansible_eth0']['ipv4']['address'] }}"

analytics_master_db_ips:
  ww: '{{ww_analytics_master_db_ip}}'
  xx: '{{xx_analytics_master_db_ip}}'

analytics_master_private_ips:
  ww: '{{ww_analytics_master_private_ip}}'
  xx: '{{xx_analytics_master_private_ip}}'

#
#Load Balancer IPs
#
ww_db_load_balancer_ip: "{{ hostvars[(groups['dw-pglb'] | intersect( groups[realm] ) | intersect(groups['tag_tenant_ww']) | list )[0]]['ansible_eth0']['ipv4']['address'] }}"
xx_db_load_balancer_ip: "{{ hostvars[(groups['dw-pglb'] | intersect( groups[realm] ) | intersect(groups['tag_tenant_xx']) | list )[0]]['ansible_eth0']['ipv4']['address'] }}"

db_load_balancer_ips:
  ww: '{{ww_db_load_balancer_ip}}'
  xx: '{{xx_db_load_balancer_ip}}'

ww_cds_load_balancer_ip: "{{ hostvars[(groups['cds-pglb'] | intersect( groups[realm] ) | intersect(groups['tag_tenant_ww']) | list )[0]]['ansible_eth0']['ipv4']['address'] }}"
xx_cds_load_balancer_ip: "{{ hostvars[(groups['cds-pglb'] | intersect( groups[realm] ) | intersect(groups['tag_tenant_xx']) | list )[0]]['ansible_eth0']['ipv4']['address'] }}"

cds_tenant_load_balancer_ips:
  ww: '{{ww_cds_load_balancer_ip}}'
  xx: '{{xx_cds_load_balancer_ip}}'

cds_parcc_load_balancer_ip: "{{ hostvars[(groups['cds-pglb-parcc'] | intersect( groups[realm] ) | list )[0]]['ansible_eth0']['ipv4']['address'] }}"

#
#Analytics Load Balancer IPs and Data Sources
#
analytics_ww_db_load_balancer_ip: "{{ (groups['analytics-pglb'] | intersect( groups[realm] ) | intersect(groups['tag_tenant_ww']) | list )[0] }}"
analytics_xx_db_load_balancer_ip: "{{ (groups['analytics-pglb'] | intersect( groups[realm] ) | intersect(groups['tag_tenant_xx']) | list )[0] }}"

analytics_data_sources:
  - { tenant: 'ww', db: '{{analytics_ww_db_load_balancer_ip}}', label: 'WW' }
  - { tenant: 'xx', db: '{{analytics_xx_db_load_balancer_ip}}', label: 'XX' }

#
#Production DB Schemas
#
prod_db_schemas:
  ww: edware_prod
  xx: edware_prod

#
#Reporting App related vars
#
reporting_app_server_cname: "{{ orchestrated_env_realm }}-reporting-app-server1"
reporting_app_server_pvtdns_hostname: "{{ reporting_app_server_cname ~ pvt_hosted_zone }}"
reporting_db_ip: "{{ hostvars[(groups['reporting-db-server'] | intersect( groups[realm] ) | list )[0]]['ansible_eth0']['ipv4']['address'] }}"

reporting_db_schemas:
  ww: edware_prod
  xx: edware_prod

#Celery configuration
edware_stats_db_url: "postgresql+psycopg2://{{edware_stats_user}}:{{edware_stats_password}}@{{edware_stats_db_host}}:5432/edware_stats"
edware_db_ww_url: "postgresql+psycopg2://{{master_db_user}}:{{master_db_password}}@{{db_load_balancer_ips['ww']}}:9999/edware"
edware_db_ww_schema_name: "{{prod_db_schemas['ww']}}"
edware_db_xx_url: "postgresql+psycopg2://{{master_db_user}}:{{master_db_password}}@{{db_load_balancer_ips['xx']}}:9999/edware"
edware_db_xx_schema_name: "{{prod_db_schemas['xx']}}"
edware_cds_db_ww_url: "postgresql+psycopg2://{{cds_db_user}}::{{cds_db_password}}@{{cds_tenant_load_balancer_ips['ww']}}:9999/edware"
edware_cds_db_ww_schema_name: "edware_tenant_cds"
edware_cds_db_xx_url: "postgresql+psycopg2://{{cds_db_user}}::{{cds_db_password}}@{{cds_tenant_load_balancer_ips['xx']}}:9999/edware"
edware_cds_db_xx_schema_name: "edware_tenant_cds"
auth_saml_idp_server_login_url: "{{idp_login_url}}"
auth_saml_idp_server_logout_url: "{{idp_logout_url}}"
auth_saml_issuer_name: "https://{{reporting_server_address}}/sp.xml"
auth_saml_name_qualifier: "{{idp_name_qualifier}}"
cache_public_shortlived_url: "{{memcached_hosts}}"
cache_public_data_url: "{{memcached_hosts}}"
cache_session_url: "{{memcached_hosts}}"
cache_public_filtered_data_url: "{{memcached_hosts}}"
extract_celery_BROKER_URL: "amqp://{{extract_celery_user}}:{{extract_celery_password}}@{{extract_queue_ip}}/{{extract_queue_vhost}}"
services_celery_BROKER_URL: "amqp://{{pdf_services_celery_user}}:{{pdf_services_celery_password}}@{{pdf_services_queue_ip}}/{{pdf_services_queue_vhost}}"
analytics_url: "https://{{pentaho_server_address}}/pentaho"
hpz_file_registration_url: "http://{{ hpz_app_server_pvtdns_hostname }}:{{ hpz_frs_port }}/registration"
hpz_file_upload_base_url: "http://{{ hpz_app_server_pvtdns_hostname }}:{{ hpz_frs_port }}/files"
pdf_base_uri: "https://{{reporting_server_address}}/assets/html/"
extract_work_zone_base_dir: "/opt/edware/extraction"

reporting_server_configuration:
    - { option: 'auth.policy.secure', value: "True"}
    - { option: 'auth.skip.verify', value: "True"}
    - { option: 'edware.cds.db.parcc.url', value: "postgresql+psycopg2://{{cds_db_user}}::{{cds_db_password}}@{{cds_parcc_load_balancer_ip}}:9999/edware"}
    - { option: 'edware.cds.db.parcc.schema_name', value: "edware_cds"}
    - { option: 'trigger.recache.enable', value: "{{trigger_recache_enable | default('False') }}"}
    - { option: 'trigger.pdf.enable', value: "{{trigger_pdf_enable | default('False') }}"}
    - { option: 'services.proxy_url', value: "http://{{proxy_host}}:{{proxy_port}}"}

#
# Extract Variables
#
extract_server_configuration:
    - { option: 'edware.cds.db.parcc.url', value: "postgresql+psycopg2://{{cds_db_user}}::{{cds_db_password}}@{{cds_parcc_load_balancer_ip}}:9999/edware"}
    - { option: 'edware.cds.db.parcc.schema_name', value: "edware_cds"}

#
#HPZ App related vars
#
hpz_app_server_cname: "{{ orchestrated_env_realm }}-hpz-app-server1"
hpz_app_server_pvtdns_hostname: "{{ hpz_app_server_cname ~ pvt_hosted_zone }}"
hpz_frs_port: 8080

hpz_server_configuration:
    - { option: 'auth.saml.issuer_name', value: "https://{{hpz_server_address}}/sp.xml"}
    - { option: 'auth.saml.name_qualifier', value: "{{idp_name_qualifier}}"}
    - { option: 'auth.saml.idp_server_login_url', value: "{{idp_login_url}}"}
    - { option: 'auth.saml.idp_server_logout_url', value: "{{idp_logout_url}}"}
    - { option: 'auth.policy.secure', value: "True"}
    - { option: 'hpz.db.url', value: "postgresql+psycopg2://{{hpz_db_user}}:{{hpz_db_password}}@{{hpz_db_host}}:5432/hpz"}
    - { option: 'hpz.frs.download_base_url', value: "https://{{hpz_server_address}}/download"}
    - { option: 'cache.session.url', value: "{{memcached_hosts}}"}

#
# CDS Configurations
#
cds_parcc_migration_configuration:
    - { option: 'cds.consortium.migrate_dest.db.parcc.schema_name', value: 'edware_cds' }
    - { option: 'cds.consortium.migrate_dest.db.parcc.url', value: 'postgresql+psycopg2://{{cds_db_user}}:{{cds_db_password}}@{{parcc_cds_private_ip}}:5432/edware' }

    - { option: 'cds.consortium.migrate_source.db.ww.schema_name', value: 'edware_tenant_cds' }
    - { option: 'cds.consortium.migrate_source.db.ww.url', value: "postgresql+psycopg2://{{cds_db_user}}:{{cds_db_password}}@{{cds_private_ips['ww']}}:5432/edware" }

    - { option: 'cds.consortium.migrate_source.db.xx.schema_name', value: 'edware_tenant_cds' }
    - { option: 'cds.consortium.migrate_source.db.xx.url', value: "postgresql+psycopg2://{{cds_db_user}}:{{cds_db_password}}@{{cds_private_ips['xx']}}:5432/edware" }

cds_tenant_migration_configuration:
    - { option: 'cds.tenant.migrate_dest.db.{{current_tenant}}.schema_name', value: 'edware_tenant_cds'}
    - { option: 'cds.tenant.migrate_dest.db.{{current_tenant}}.url', value: 'postgresql+psycopg2://{{cds_db_user}}:{{cds_db_password}}@{{cds_private_ips[current_tenant]}}:5432/edware'}

    - { option: 'cds.tenant.migrate_source.db.{{current_tenant}}.schema_name', value: 'edware_prod'}
    - { option: 'cds.tenant.migrate_source.db.{{current_tenant}}.url', value: 'postgresql+psycopg2://{{master_db_user}}:{{master_db_password}}@{{master_private_ips[current_tenant]}}:5432/edware'}

#
#Starmigrate DB Schemas
#
starmigrate_db_schemas:
  ww: starmigrate
  xx: starmigrate

analytics_db_schemas:
  ww: analytics
  xx: analytics

#
#Pentaho related vars
#
pentaho_jvm_parameters:
  - '-Xms4g'
  - '-Xmx26g'
  - '-XX:MetaspaceSize=512m'
  - '-XX:MaxMetaspaceSize=512m'

elb_present: true
url_protocol: https

#
#State Codes
#
state_codes:
  - 'WW'
  - 'XX'

#
#RabbitMQ Clustering
#
rabbit_erlang_cookie: JDLANTRJSXFERPLHRXQU
sensu_rabbit_erlang_cookie: YNDDHZWBDLHSRAZNIAMZ

#
#E2E Heartbeat Monitoring
#
e2e_monitoring_topics:
  - e2e-heartbeats

web_heartbeat_port: 443
pentaho_heartbeat_port: 443

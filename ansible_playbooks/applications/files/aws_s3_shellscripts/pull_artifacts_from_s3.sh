#!/bin/bash

# Requirements:
# Options:
# 	BUCKET: 
#		Usage:	 BUCKET=<S3 Bucket name>
#		Desc:  	 Bucket to pull from. 	
# 	FOLDER:  
#		Usage:	 FOLDER=<Folder name>
#		Desc:  	 Folder to pull from. 	
#		Default: Default latest folder is choosen if none provided
# 	FILE:  
#		Usage:	 FILE=<File name>
#		Desc:  	 File to pull from the folder. 	
#		Default: Default all files in the folder, if none provided.
# 	DEST: 
#		Usage:	 DEST=<Destination>
#		Desc:  	 Location to store the files pulled. 	
#		Default: Default is current location 
#
# eg: 
# sh parcc_devops-pull_artifacts_from_s3.sh BUCKET=<BUCKET NAME> 
#		This will download the latest folder and all its files from the bucket to current location
# sh parcc_devops-pull_artifacts_from_s3.sh BUCKET=<BUCKET NAME> FOLDER=<Folder Name>
#		This will download the folder from the bucket to current location
# sh parcc_devops-pull_artifacts_from_s3.sh BUCKET=<BUCKET NAME> FILE=<File Name>
#		This will download the file from the latest folder in the bucket to current location
# sh parcc_devops-pull_artifacts_from_s3.sh BUCKET=<BUCKET NAME> FOLDER=<Folder Name> FILE=<File Name>
#		This will download the file from the provided folder in the bucket to current location
# sh parcc_devops-pull_artifacts_from_s3.sh BUCKET=<BUCKET NAME> DEST=<DESTINATION FOLDER>
#		This will download the latest folder and all its files from the bucket to destination location


PFS_FOLDER=""
PFS_BUCKET=""
PFS_FILE=""
PFS_DEST="."
PFS_S3COMMAND="sync"
PFS_LATEST=true

get_folder_int_value() {
	local pfs_fn_no_hyphens="${pfs_folder_name//-}"
	local pfs_fn_no_underscores="${pfs_fn_no_hyphens//_}"
	echo "$pfs_fn_no_underscores"		
}

get_folder_name() {
	echo "${pfs_folder///}"
}

for arg in "$@"
do
	pair=(${arg//=/ })
	
	if [ "${pair[0]}" == "FOLDER" ] ; then
		PFS_FOLDER="${pair[1]}"
		PFS_LATEST=false
	fi

	if [ "${pair[0]}" == "BUCKET" ] ; then
		PFS_BUCKET="${pair[1]}"
	fi

	if [ "${pair[0]}" == "FILE" ] ; then
		PFS_FILE="${pair[1]}"
		PFS_S3COMMAND="cp"
	fi

	if [ "${pair[0]}" == "DEST" ] ; then
		PFS_DEST="${pair[1]}"
	fi

	unset pair
done

if [ "$PFS_BUCKET" == "" ]; then 
	echo "Required option bucket is not provided."
	echo "ex usage: sh parcc_devops-pull_artifacts_from_s3.sh BUCKET=<BUCKET NAME>"
	echo "   This will download the latest folder from the provided bucket to current folder"
else 
	if $PFS_LATEST ; then
		PFS_FOLDERS=($(aws s3 ls s3://$PFS_BUCKET))
		pfs_max="0"
	
		for pfs_folder in "${PFS_FOLDERS[@]}" ; do
			if [ "$pfs_folder" != "PRE" ] ; then
				pfs_folder_name=$(get_folder_name)
				pfs_folder_int_value=$(get_folder_int_value)
				((pfs_folder_int_value > pfs_max)) && pfs_max=$pfs_folder_int_value && PFS_FOLDER=$pfs_folder_name
	    	fi
		done

		unset pfs_max
		unset pfs_folder
		unset pfs_folder_name
		unset pfs_folder_int_value

		echo "Latest folder is $PFS_FOLDER"
	fi

	aws s3 $PFS_S3COMMAND s3://$PFS_BUCKET/$PFS_FOLDER/$PFS_FILE $PFS_DEST

	unset PFS_FOLDER
	unset PFS_BUCKET
	unset PFS_FILE
	unset PFS_DEST
	unset PFS_S3COMMAND
	unset PFS_LATEST
fi
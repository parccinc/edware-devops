#!/bin/bash

# Requirements:
# The lower environment should provided list and get access to the upper environment.
# This should be set on every lower environment bucket under bucket policies
# Here are couple of links for reference
#          http://docs.aws.amazon.com/AmazonS3/latest/dev/using-iam-policies.html
#          http://docs.aws.amazon.com/IAM/latest/UserGuide/AccessPolicyLanguage_ElementDescriptions.html#Principal
# 
# ex policy looks like :
#
# {
#   "Id": "Policy1413565839802",
#   "Statement": [
#     {
#       "Sid": "Stmt1413561083699",
#       "Action": [
#         "s3:ListBucket"
#       ],
#       "Effect": "Allow",
#       "Resource": "arn:aws:s3:::<UPPER ENVIRONMENT ACCOUNT ID>",
#       "Principal": {
#         "AWS": [
#           "<UPPER ENVIRONMENT ACCOUNT ID>"
#         ]
#       }
#     },
#     {
#       "Sid": "Stmt1413561117937",
#       "Action": [
#         "s3:GetObject"
#       ],
#       "Effect": "Allow",
#       "Resource": "arn:aws:s3:::<UPPER ENVIRONMENT ACCOUNT ID>/*",
#       "Principal": {
#         "AWS": [
#           "<UPPER ENVIRONMENT ACCOUNT ID>"
#         ]
#       }
#     }
#   ]
# }

ENVIRONMENT=""
RELEASE=""
FOLDER=""
YUMVERSION=""

for arg in "$@"
do
  pair=(${arg//=/ })
  
  if [ "${pair[0]}" == "ENVIRONMENT" ] ; then
    ENVIRONMENT="${pair[1]}"
  fi

  if [ "${pair[0]}" == "RELEASE" ] ; then
    RELEASE="${pair[1]}"
  fi

  if [ "${pair[0]}" == "FOLDER" ] ; then
    FOLDER="${pair[1]}"
  fi
  
  if [ "${pair[0]}" == "YUMVERSION" ] ; then
    YUMVERSION="${pair[1]}"
  fi
  unset pair
done

# If Environment is not provided as an argument to the script, we need to find it out
# In order to find out which environment we are in, we find out the VPC of the aws ec2 instance.
# Each environment has different VPC. 
if [ -z "$ENVIRONMENT" ] ; then
    echo "Environment not provided. Finding environment using VPC"
   
    PFL_DEV_VPC_ID="vpc-9f60b3fa"
    PFL_QA_VPC_ID="vpc-0af2706f"
    PFL_STG_VPC_ID="vpc-9fb33ffa"
    PFL_PROD_VPC_ID="vpc-717ac015"
    
    PFL_CURR_VPC_ID=`aws ec2 describe-vpcs --query "Vpcs[0].VpcId" | sed -e 's/\"//g'`
    
    if [ "$PFL_CURR_VPC_ID" == "$PFL_DEV_VPC_ID" ] ; then
        ENVIRONMENT="DEV"
        echo "Found environment as DEV"
    elif [ "$PFL_CURR_VPC_ID" == "$PFL_QA_VPC_ID" ] ; then
        ENVIRONMENT="QA"
        echo "Found environment as QA"
    elif [ "$PFL_CURR_VPC_ID" == "$PFL_STG_VPC_ID" ] ; then
        ENVIRONMENT="STG"
        echo "Found environment as STG"
    elif [ "$PFL_CURR_VPC_ID" == "$PFL_PROD_VPC_ID" ] ; then
        ENVIRONMENT="PROD"
        echo "Found environment as PROD"
    else
        echo "invalid  or unknown environment entirely....please check with OPs ....exitting now"
        exit 113
    fi

    unset PFL_DEV_VPC_ID
    unset PFL_QA_VPC_ID
    unset PFL_STG_VPC_ID
    unset PFL_PROD_VPC_ID
    unset PFL_CURR_VPC_ID
fi

if [ "$ENVIRONMENT" == "" ] ; then
   echo "Unable to determine the environment"
elif [ "$ENVIRONMENT" == "DEV" ] ; then
   echo "There is no lower environment to pull from"
elif [ "$ENVIRONMENT" == "QA" ] ; then
   echo "QA : Pulling from DEV bucket amplify-nightly-artifacts-${RELEASE} to QA bucket amplify-artifacts-qa-${RELEASE}"
   aws s3 sync s3://amplify-nightly-artifacts-dev-${RELEASE} s3://amplify-artifacts-qa-${RELEASE}
   WORKDIR=$(dirname $0)
   $WORKDIR/yum_bucket_update_by_version.sh
   
elif [ "$ENVIRONMENT" == "STG" ] ; then
   echo "STG : Pulling from QA bucket amplify-release-artifacts-qa-${RELEASE}/${FOLDER} to STG bucket amplify-artifacts-stg-${RELEASE}/${FOLDER}"
   aws s3 sync s3://amplify-release-artifacts-qa-${RELEASE}/${FOLDER} s3://amplify-artifacts-stg-${RELEASE}/${FOLDER}
   WORKDIR=$(dirname $0)
   $WORKDIR/yum_bucket_update_by_version.sh ${YUMVERSION}
   
elif [ "$ENVIRONMENT" == "PROD" ] ; then
   echo "PROD : Pulling from STG bucket amplify-release-artifacts-stg-${RELEASE}/${FOLDER} to PROD bucket parcc-artifacts-prod-${RELEASE}/${FOLDER}"
   aws s3 sync s3://amplify-release-artifacts-stg-${RELEASE}/${FOLDER} s3://parcc-artifacts-prod-${RELEASE}/${FOLDER}
   WORKDIR=$(dirname $0)
   $WORKDIR/yum_bucket_update_by_version.sh ${YUMVERSION}
else 
   echo "No operations found for environment : $ENVIRONMENT"
fi


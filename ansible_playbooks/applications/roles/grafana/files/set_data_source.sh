#!/bin/bash

PGPASSWORD=$1 psql -U $2 -d $3 -h $4 \
-c "INSERT INTO data_source (id, org_id, version, type, name, access, url, basic_auth, is_default, json_data, created, updated) \
VALUES (1, 1, 0, 'graphite', 'Graphite Data Source', 'proxy', 'http://admin:admin@127.0.0.1:8080', FALSE, TRUE, null, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);"

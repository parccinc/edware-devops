#!/bin/bash

SearchString="Match Group $1"
Results=`grep -o "${SearchString}" "$2"`

if [[ -z "$Results" ]] ; then
	echo "Match Group $1" >> $2
	echo "       X11Forwarding no" >> $2
	echo "       AllowTcpForwarding no" >> $2
	echo "       ForceCommand internal-sftp" >> $2
	echo "       ChrootDirectory /sftp/%h" >> $2
fi

#! /bin/bash

set -e

cd /tmp
tar xzf mod_wsgi-3.5.tar.gz
cd mod_wsgi-3.5/
./configure --with-python=/usr/local/bin/python3.3
make
make install
cd /tmp
rm -R mod_wsgi*

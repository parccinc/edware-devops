#!/bin/bash

source /opt/virtualenv/reporting/bin/activate
cd /opt/edware/conf
python generate_ini.py -e $1 -i settings.yaml -o /opt/edware/conf/reporting.ini

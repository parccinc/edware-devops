#!/bin/bash

source /opt/virtualenv/udl2/bin/activate
cd /opt/virtualenv/udl2/lib/python3.3/site-packages/edschema-0.1-py3.3.egg/edschema
python metadata_generator.py -s $1 -d edware -m edware --host $2 -p $3 -a $4
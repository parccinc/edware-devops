#!/bin/bash

source /opt/virtualenv/udl2/bin/activate
cd /opt/virtualenv/udl2
# TEMPORARY
cd /opt/virtualenv/udl2/lib/python3.3/site-packages/edudl2-0.1-py3.3.egg/edudl2/database/
python database.py --action $1 --tenant $2
# python -m edudl2.database.database --action $1 --tenant $2
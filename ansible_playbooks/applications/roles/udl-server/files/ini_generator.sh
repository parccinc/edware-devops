#!/bin/bash

source /opt/virtualenv/udl2/bin/activate
cd /opt/edware/conf
python generate_ini.py -e $1 -i udl2_conf.yaml -o /opt/edware/conf/udl2_conf.ini
python generate_ini.py -e $1 -i settings.yaml -o /opt/edware/conf/smarter.ini

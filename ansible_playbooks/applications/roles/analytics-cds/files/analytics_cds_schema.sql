--
-- PostgreSQL analytics_cds Schema
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 7 (class 2615 OID 18898)
-- Name: analytics_cds; Type: SCHEMA; Schema: -; Owner: edware
--

DROP SCHEMA IF EXISTS analytics_cds CASCADE;
CREATE SCHEMA analytics_cds;


ALTER SCHEMA analytics_cds OWNER TO edware;

SET search_path = analytics_cds, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 177 (class 1259 OID 18931)
-- Name: dim_poy; Type: TABLE; Schema: analytics_cds; Owner: edware; Tablespace: 
--

CREATE TABLE dim_poy (
    poy_key numeric(4,0) DEFAULT (-1) NOT NULL,
    poy character varying(20) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    end_year character varying(9) DEFAULT 'UNKNOWN'::character varying NOT NULL,
    school_year character(9) DEFAULT 'UNKNOWN'::bpchar NOT NULL,
    last_insert_date date DEFAULT ('now'::text)::date NOT NULL,
    valid_from date DEFAULT '1900-01-01'::date NOT NULL,
    valid_to date DEFAULT '2199-12-31'::date NOT NULL,
    rec_version smallint NOT NULL
);


ALTER TABLE dim_poy OWNER TO edware;

--
-- TOC entry 2429 (class 0 OID 0)
-- Dependencies: 177
-- Name: COLUMN dim_poy.poy_key; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN dim_poy.poy_key IS 'YEAR || 01 OR YEAR || 02';


--
-- TOC entry 2430 (class 0 OID 0)
-- Dependencies: 177
-- Name: COLUMN dim_poy.poy; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN dim_poy.poy IS 'Fall, Spring';


--
-- TOC entry 2431 (class 0 OID 0)
-- Dependencies: 177
-- Name: COLUMN dim_poy.end_year; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN dim_poy.end_year IS 'Academioc year end year,example 2014 for 2013-2014';


--
-- TOC entry 178 (class 1259 OID 18941)
-- Name: dim_student; Type: TABLE; Schema: analytics_cds; Owner: edware; Tablespace: 
--

CREATE TABLE dim_student (
    student_key numeric(7,0) NOT NULL,
    student_sex character(1),
    ethnicity character varying(150) DEFAULT 'could not resolve'::character varying NOT NULL,
    ell character(1),
    lep_status character(1),
    econo_disadvantage character(1),
    disabil_student character(1),
    last_insert_date date DEFAULT ('now'::text)::date NOT NULL,
    rec_version smallint NOT NULL,
    valid_from timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    valid_to timestamp without time zone DEFAULT '2199-12-31 23:59:59.999'::timestamp without time zone
);


ALTER TABLE dim_student OWNER TO edware;

--
-- TOC entry 2441 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN dim_student.ell; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN dim_student.ell IS 'Parcc name:English Language Learner ELL. English Language Learner (ELL).';


--
-- TOC entry 2442 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN dim_student.lep_status; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN dim_student.lep_status IS 'Ampliofy name:LEPStatus, Parcc name:Limited English Proficient (LEP)	sed to indicate persons (A) who are ages 3 through 21; (B) who are enrolled or preparing to enroll in an elementary school or a secondary school; (C ) (who are I, ii, or iii) (i) who were not born in the United States or whose native languages are languages other than English; (ii) (who are I and II) (I) who are a Native American or Alaska Native, or a native resident of the outlying areas; and (II) who come from an environment where languages other than English have a significant impact on their level of language proficiency; or (iii) who are migratory, whose native languages are languages other than English, and who come from an environment where languages other than English are dominant; and (D) whose difficulties in speaking, reading, writing, or understanding the English language may be sufficient to deny the individuals (who are denied I or ii or iii) (i) the ability to meet the state''s proficient level of achievement on state assessments described in section 1111(b)(3); (ii) the ability to successfully achieve in classrooms where the language of instruction is English; or (iii) the opportunity to participate fully in society.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20783';


--
-- TOC entry 2445 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN dim_student.econo_disadvantage; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN dim_student.econo_disadvantage IS 'Parcc field: Economic Disadvantage Status. An indication that the student met the State criteria for classification as having an economic disadvantage. https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=20741';


--
-- TOC entry 2446 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN dim_student.disabil_student; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN dim_student.disabil_student IS 'Parcc name:Student With Disability. An indication of whether a person is classified as disabled under the American''s with Disability Act (ADA).https://ceds.ed.gov/CEDSElementDetails.aspx?TermId=3569\A0';


--
-- TOC entry 180 (class 1259 OID 18967)
-- Name: dim_unique_test; Type: TABLE; Schema: analytics_cds; Owner: edware; Tablespace: 
--

CREATE TABLE dim_unique_test (
    test_key numeric NOT NULL,
    test_code character varying(20),
    test_subject character varying(35),
    asmt_grade character varying(12)
);


ALTER TABLE dim_unique_test OWNER TO edware;

--
-- TOC entry 181 (class 1259 OID 18973)
-- Name: fact_sum; Type: TABLE; Schema: analytics_cds; Owner: edware; Tablespace: 
--

CREATE TABLE fact_sum (
    rec_id bigint NOT NULL,
    poy_key numeric(4,0) DEFAULT (-1) NOT NULL,
    student_key integer NOT NULL,
    student_grade character varying(12) NOT NULL,
    sum_scale_score integer,
    sum_csem integer,
    sum_perf_lvl smallint,
    sum_read_scale_score integer,
    sum_write_scale_score integer,
    subclaim1_category smallint,
    subclaim2_category smallint,
    subclaim3_category smallint,
    subclaim4_category smallint,
    subclaim5_category smallint,
    subclaim6_category smallint,
    parcc_growth_percent numeric(5,2),
    include_in_parcc character varying(5) DEFAULT false NOT NULL,
    include_in_isr character varying(5) DEFAULT false NOT NULL,
    include_in_roster character varying(5) DEFAULT 'N'::bpchar NOT NULL,
    create_date date DEFAULT ('now'::text)::date NOT NULL,
    me_flag character varying(8),
    sum_score_rec_uuid character varying(36),
    summative_population integer DEFAULT 0,
    registered_population integer DEFAULT 1,
    summative_proficient_population integer,
    unique_test_key integer
);


ALTER TABLE fact_sum OWNER TO edware;

--
-- TOC entry 2457 (class 0 OID 0)
-- Dependencies: 181
-- Name: COLUMN fact_sum.poy_key; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN fact_sum.poy_key IS 'YEAR || 01 OR YEAR || 02';


--
-- TOC entry 2458 (class 0 OID 0)
-- Dependencies: 181
-- Name: COLUMN fact_sum.student_grade; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN fact_sum.student_grade IS 'The typical grade or combination of grade-levels, developmental levels, or age-levels for which an assessment is designed. (multiple selections supported)	. KG, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, PS.https://ceds.ed.gov/CEDSElementDetails.aspx?TermxTopicId=21362';


--
-- TOC entry 2459 (class 0 OID 0)
-- Dependencies: 181
-- Name: COLUMN fact_sum.sum_scale_score; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score';


--
-- TOC entry 2461 (class 0 OID 0)
-- Dependencies: 181
-- Name: COLUMN fact_sum.sum_perf_lvl; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_perf_lvl IS 'Summative Performance Level';


--
-- TOC entry 2462 (class 0 OID 0)
-- Dependencies: 181
-- Name: COLUMN fact_sum.sum_read_scale_score; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_read_scale_score IS 'Summative Reading Scale Score';


--
-- TOC entry 2464 (class 0 OID 0)
-- Dependencies: 181
-- Name: COLUMN fact_sum.sum_write_scale_score; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN fact_sum.sum_write_scale_score IS 'Amplify name: AssessmentSubtestScaledScore, pearson name: Test Scaled Score';


--
-- TOC entry 2466 (class 0 OID 0)
-- Dependencies: 181
-- Name: COLUMN fact_sum.subclaim1_category; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim1_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- TOC entry 2467 (class 0 OID 0)
-- Dependencies: 181
-- Name: COLUMN fact_sum.subclaim2_category; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim2_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- TOC entry 2468 (class 0 OID 0)
-- Dependencies: 181
-- Name: COLUMN fact_sum.subclaim3_category; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim3_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- TOC entry 2469 (class 0 OID 0)
-- Dependencies: 181
-- Name: COLUMN fact_sum.subclaim4_category; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim4_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- TOC entry 2470 (class 0 OID 0)
-- Dependencies: 181
-- Name: COLUMN fact_sum.subclaim5_category; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim5_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- TOC entry 2471 (class 0 OID 0)
-- Dependencies: 181
-- Name: COLUMN fact_sum.subclaim6_category; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN fact_sum.subclaim6_category IS 'Subclaim 1 Category. 1 = At or Above Students at level 4 2 = Near Students at level 4 3 = Below Students at level 4 Blank. Subclaim 1 = Reading-RL (Reading Literature) Subclaim 1 = Major Content (Mathematics) Subclaim A Note: scale scored';


--
-- TOC entry 2474 (class 0 OID 0)
-- Dependencies: 181
-- Name: COLUMN fact_sum.parcc_growth_percent; Type: COMMENT; Schema: analytics_cds; Owner: edware
--

COMMENT ON COLUMN fact_sum.parcc_growth_percent IS 'Student Growth Percentile Compared to PARCC';


--
-- TOC entry 2251 (class 2606 OID 19005)
-- Name: dim_poy_pk; Type: CONSTRAINT; Schema: analytics_cds; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_poy
    ADD CONSTRAINT dim_poy_pk PRIMARY KEY (poy_key);


--
-- TOC entry 2254 (class 2606 OID 19007)
-- Name: dim_student_pk; Type: CONSTRAINT; Schema: analytics_cds; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_student
    ADD CONSTRAINT dim_student_pk PRIMARY KEY (student_key);


--
-- TOC entry 2260 (class 2606 OID 19011)
-- Name: dim_unique_test_pkey; Type: CONSTRAINT; Schema: analytics_cds; Owner: edware; Tablespace: 
--

ALTER TABLE ONLY dim_unique_test
    ADD CONSTRAINT dim_unique_test_pkey PRIMARY KEY (test_key);


--
-- TOC entry 2265 (class 1259 OID 19016)
-- Name: fact_sum_ix2; Type: INDEX; Schema: analytics_cds; Owner: edware; Tablespace: 
--

CREATE INDEX fact_sum_ix2 ON fact_sum USING btree (poy_key);


--
-- TOC entry 2267 (class 1259 OID 19018)
-- Name: fact_sum_ix9; Type: INDEX; Schema: analytics_cds; Owner: edware; Tablespace: 
--

CREATE INDEX fact_sum_ix9 ON fact_sum USING btree (student_key);


--
-- TOC entry 2252 (class 1259 OID 19020)
-- Name: idx_dim_poy_lookup; Type: INDEX; Schema: analytics_cds; Owner: edware; Tablespace: 
--

CREATE INDEX idx_dim_poy_lookup ON dim_poy USING btree (poy, school_year);


--
-- TOC entry 2268 (class 1259 OID 19023)
-- Name: me_idx; Type: INDEX; Schema: analytics_cds; Owner: edware; Tablespace: 
--

CREATE INDEX me_idx ON fact_sum USING btree (me_flag);


--
-- TOC entry 2275 (class 2606 OID 19038)
-- Name: fact_sum_fk4; Type: FK CONSTRAINT; Schema: analytics_cds; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fact_sum_fk4 FOREIGN KEY (student_key) REFERENCES dim_student(student_key);


--
-- TOC entry 2276 (class 2606 OID 19043)
-- Name: fact_sum_fk6; Type: FK CONSTRAINT; Schema: analytics_cds; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fact_sum_fk6 FOREIGN KEY (poy_key) REFERENCES dim_poy(poy_key);


--
-- TOC entry 2279 (class 2606 OID 19058)
-- Name: fk_unique_test; Type: FK CONSTRAINT; Schema: analytics_cds; Owner: edware
--

ALTER TABLE ONLY fact_sum
    ADD CONSTRAINT fk_unique_test FOREIGN KEY (unique_test_key) REFERENCES dim_unique_test(test_key);


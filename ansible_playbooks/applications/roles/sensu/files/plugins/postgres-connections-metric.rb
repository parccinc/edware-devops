#!/usr/bin/env ruby
#
# Postgres Connection Metrics
# ===
#
# Dependencies
# -----------
# - Ruby gem `pg`
#
#
# Copyright 2012 Kwarter, Inc <platforms@kwarter.com>
# Author Gilles Devaux <gilles.devaux@gmail.com>
#
# Released under the same terms as Sensu (the MIT license); see LICENSE
# for details.

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'socket'

require_relative 'postgres-wrap-psql.rb'

class PostgresStatsDBMetrics < Sensu::Plugin::Metric::CLI::Graphite
  option :psql,
         description: 'Path to psql binary',
         long: '--psql PATH',
         required: true

  option :hostname,
         description: 'Hostname to login to',
         short: '-h HOST',
         long: '--hostname HOST',
         default: 'localhost'

  option :port,
         description: 'Database port',
         short: '-P PORT',
         long: '--port PORT',
         default: 5432

  option :user,
         description: 'Postgres User',
         short: '-u USER',
         long: '--user USER',
         default: 'postgres'

  option :database,
         description: 'Database name',
         short: '-d DB',
         long: '--db DB'

  option :scheme,
         description: 'Metric naming scheme, text to prepend to $queue_name.$metric',
         long: '--scheme SCHEME',
         default: "#{Socket.gethostname}.postgresql"

  def run
    timestamp = Time.now.to_i
    metrics = {}
    result = []

    if config[:database]
      metrics[config[:database]] = [0, 0]
      result = run_psql(config, "select datname, waiting, count(*) from pg_stat_activity where datname = '#{config[:database]}' group by datname, waiting")
    else
      run_psql(config, "select datname from pg_catalog.pg_database").each do |db|
        metrics[db] = [0, 0]
      end
      result = run_psql(config, "select datname, waiting, count(*) from pg_stat_activity group by datname, waiting")
    end

    result.each do |row|
      res = row.split('|')

      if res[1] == 't'
        metrics[res[0]][1] = res[2]
      else
        metrics[res[0]][0] = res[2]
      end
    end

    metrics.each do |dbname, conns|
      output "#{config[:scheme].split('.')[0]}.postgresql.connections.#{dbname}.active",  conns[0], timestamp
      output "#{config[:scheme].split('.')[0]}.postgresql.connections.#{dbname}.waiting", conns[1], timestamp
    end

    ok
  end
end

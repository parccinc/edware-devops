#!/usr/bin/env ruby
#
# Postgres Alive Plugin
#
# This plugin attempts to login to postgres with provided credentials.
#
# Copyright 2012 Lewis Preson & Tom Bassindale
#
# Released under the same terms as Sensu (the MIT license); see LICENSE
# for details.

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/check/cli'

require_relative 'postgres-wrap-psql.rb'

class CheckPostgres < Sensu::Plugin::Check::CLI
  option :psql,
         description: 'Path to psql binary',
         long: '--psql PATH',
         required: true

  option :hostname,
         description: 'Hostname to login to',
         short: '-h HOST',
         long: '--hostname HOST'

  option :port,
         description: 'Database port',
         short: '-P PORT',
         long: '--port PORT',
         default: 5432

  option :user,
         description: 'Postgres User',
         short: '-u USER',
         long: '--user USER',
         default: 'postgres'

  option :database,
         description: 'Database schema to connect to',
         short: '-d DATABASE',
         long: '--database DATABASE'

  def run
    r = run_psql(config, "select 1")

    if r and r[0].to_i == 1
      ok('PostgreSQL is alive')
    else
      critical("PostgreSQL did not return expected value: #{r}")
    end
  end
end

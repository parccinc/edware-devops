#!/usr/bin/env ruby
#  encoding: UTF-8
#
# Check RabbitMQ Queue Messages
# ===
#
# DESCRIPTION:
# This plugin checks the number of messages queued on the RabbitMQ server in a specific queues
#
# PLATFORMS:
#   Linux, BSD, Solaris
#
# DEPENDENCIES:
#   RabbitMQ rabbitmq_management plugin
#   gem: sensu-plugin
#   gem: carrot-top
#
# LICENSE:
# Copyright 2012 Evan Hazlett <ejhazlett@gmail.com>
#
# Released under the same terms as Sensu (the MIT license); see LICENSE
# for details.

require 'sensu-plugin/check/cli'
require 'socket'
require 'carrot-top'

# main plugin class
class CheckRabbitMQMessages < Sensu::Plugin::Check::CLI
  option :host,
         description: 'RabbitMQ management API host',
         long: '--host HOST',
         default: 'localhost'

  option :port,
         description: 'RabbitMQ management API port',
         long: '--port PORT',
         proc: proc(&:to_i),
         default: 15_672

  option :ssl,
         description: 'Enable SSL for connection to the API',
         long: '--ssl',
         boolean: true,
         default: false

  option :user,
         description: 'RabbitMQ management API user',
         long: '--user USER',
         default: 'guest'

  option :password,
         description: 'RabbitMQ management API password',
         long: '--password PASSWORD',
         default: 'guest'

  option :queue,
         description: 'RabbitMQ queue to monitor',
         long: '--queue queue_names',
         required: false,
         proc: proc { |a| (defined? a and (a.is_a? String)) ? a.split(',') : [] },
         default: []

  option :warn,
         short: '-w NUM_MESSAGES',
         long: '--warn NUM_MESSAGES',
         description: 'WARNING message count threshold',
         default: 250

  option :critical,
         short: '-c NUM_MESSAGES',
         long: '--critical NUM_MESSAGES',
         description: 'CRITICAL message count threshold',
         default: 500

  def rabbit
    begin
      connection = CarrotTop.new(
        host: config[:host],
        port: config[:port],
        user: config[:user],
        password: config[:password],
        ssl: config[:ssl]
      )
    rescue
      warning 'could not connect to rabbitmq'
    end
    connection
  end

  def return_condition(missing, total)
    if missing.count > 0
      message = ''
      message << "Queues missing: #{missing.join(', ')}" if missing.count > 0
      critical(message)
    elsif total >= config[:critical].to_i
      critical("#{total} exceeds queued message critical threshold.")
    elsif total >= config[:warn].to_i
      warning("#{total} exceeds queued message warning threshold.")
    else
      ok
    end
  end

  def run
    missing = config[:queue]
    total = 0

    rabbit.queues.each do |queue|
      next unless config[:queue].count < 1 or config[:queue].include?(queue['name'])
      missing.delete(queue['name'])
      total += queue['messages'] unless queue['messages'].nil?
    end

    return_condition(missing, total)
  end
end

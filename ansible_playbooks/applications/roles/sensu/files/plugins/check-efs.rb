#! /usr/bin/env ruby

require 'sensu-plugin/check/cli'
require 'aws-sdk'

class CheckEFS < Sensu::Plugin::Check::CLI
  option :aws_access_key,
         short:       '-a AWS_ACCESS_KEY',
         long:        '--aws-access-key AWS_ACCESS_KEY',
         description: "AWS Access Key. Either set ENV['AWS_ACCESS_KEY'] or provide it as an option",
         default:     ENV['AWS_ACCESS_KEY']

  option :aws_secret_access_key,
         short:       '-k AWS_SECRET_KEY',
         long:        '--aws-secret-access-key AWS_SECRET_KEY',
         description: "AWS Secret Access Key. Either set ENV['AWS_SECRET_KEY'] or provide it as an option",
         default:     ENV['AWS_SECRET_KEY']

  option :aws_region,
         short:       '-r AWS_REGION',
         long:        '--aws-region REGION',
         description: 'AWS Region (defaults to us-east-1).',
         default:     'us-east-1'

  option :creation_token,
         short:       '-t TOKEN',
         long:        '--creation-token TOKEN',
         description: 'EFS creation token'

  def run
    efs = Aws::EFS::Client.new(
      region: config[:aws_region],
      access_key_id: config[:aws_access_key],
      secret_access_key: config[:aws_secret_access_key]
    )

    resp = efs.describe_file_systems({
      max_items: 1,
      creation_token: config[:creation_token],
    })

    if resp.file_systems.any?
      ok "EFS is available!"
    else
      critical "EFS is unavailable!"
    end
  end
end

#!/usr/bin/env ruby

require 'shellwords'

def run_psql(config, query = "select 1")
  cmd = config[:psql]
  cmd << " -h " + Shellwords.escape(config[:hostname])
  if config[:port]
    cmd << " -p " + Shellwords.escape(config[:port])
  end
  cmd << " -U " + Shellwords.escape(config[:user])
  if config[:database]
    cmd << " -d " + Shellwords.escape(config[:database])
  else
    cmd << " -d postgres"
  end

  cmd << " -At -c " + Shellwords.escape(query)

  return `#{cmd}`.lines.map { |v| v.chomp }
end

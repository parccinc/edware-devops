#! /usr/bin/env ruby
#  encoding: UTF-8
#
#   cpu-metrics
#
# DESCRIPTION:
#
# OUTPUT:
#   metric data
#
# PLATFORMS:
#   Linux
#
# DEPENDENCIES:
#   gem: sensu-plugin
#   gem: socket
#
# USAGE:
#
# NOTES:
#
# LICENSE:
#   Copyright 2012 Sonian, Inc <chefs@sonian.net>
#   Released under the same terms as Sensu (the MIT license); see LICENSE
#   for details.
#
require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'socket'

class CpuGraphite < Sensu::Plugin::Metric::CLI::Graphite
  option :scheme,
         description: 'Metric naming scheme, text to prepend to metric',
         short: '-s SCHEME',
         long: '--scheme SCHEME',
         default: "#{Socket.gethostname}.cpu"

  option :sleep,
         long: '--sleep SLEEP',
         proc: proc(&:to_f),
         default: 1

  def acquire_cpu_stats
    File.open('/proc/stat', 'r').each_line do |line|
      info = line.split(/\s+/)
      name = info.shift
      return info.map(&:to_f) if name =~ /^cpu$/
    end
  end

  def run
    cpu_stats_before = acquire_cpu_stats
    sleep config[:sleep]
    cpu_stats_after = acquire_cpu_stats
    cpu_stats_substracted = cpu_stats_after.zip(cpu_stats_before).map{ |a, b| a - b }
    cpu_stats_total = cpu_stats_substracted.inject(0){ |sum,x| sum + x }
    cpu_stats_percents = []
    cpu_stats_substracted.each do |i|
      cpu_stats_percents << 100 * i / cpu_stats_total
    end
    cpu_usage_total = (cpu_stats_percents.inject(0){ |sum, x| sum + x } - cpu_stats_percents[3]).round
    output "#{config[:scheme].split('.')[0]}.cpu.cpu_usage_total", cpu_usage_total

    ok
  end
end

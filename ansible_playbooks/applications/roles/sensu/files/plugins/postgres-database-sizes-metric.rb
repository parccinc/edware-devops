#!/usr/bin/env ruby
#
# Postgres Database Sizes Metric
# ==============================
#
# Dependencies
# ------------
# - Ruby gem `pg`
#

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'socket'

require_relative 'postgres-wrap-psql.rb'

class PostgresStatsDBMetrics < Sensu::Plugin::Metric::CLI::Graphite
  option :psql,
         description: 'Path to psql binary',
         long: '--psql PATH',
         required: true

  option :hostname,
         description: 'Hostname to login to',
         short: '-h HOST',
         long: '--hostname HOST',
         default: 'localhost'

  option :port,
         description: 'Database port',
         short: '-P PORT',
         long: '--port PORT',
         default: 5432

  option :user,
         description: 'Postgres User',
         short: '-u USER',
         long: '--user USER',
         default: 'postgres'

  option :scheme,
         description: 'Metric naming scheme, text to prepend to $queue_name.$metric',
         long: '--scheme SCHEME',
         default: "#{Socket.gethostname}.postgresql"

  def run
    timestamp = Time.now.to_i

    run_psql(config, "select p.datname, pg_database_size(p.datname) as dbsize from pg_catalog.pg_database p order by p.datname asc").each do |result|
      row = result.split('|')
      output "#{config[:scheme].split('.')[0]}.postgresql.size.database.#{row[0]}", row[1], timestamp
    end

    ok
  end
end

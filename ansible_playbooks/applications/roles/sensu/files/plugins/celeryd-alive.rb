#!/usr/bin/env ruby
#
# celeryd check alive plugin
# ===
#
# DESCRIPTION:
# This plugin checks if the celeryd dameon is alive using the `celery status` command.
#

require 'sensu-plugin/check/cli'
require 'shellwords'

class CheckCelerydAlive < Sensu::Plugin::Check::CLI
  option :path,
         description: 'Path to python bin directory',
         short: '-x',
         long: '--path PATH'

  option :host,
         description: 'RabbitMQ host',
         short: '-h',
         long: '--host HOST',
         default: 'localhost'

  option :username,
         description: 'RabbitMQ username',
         short: '-u',
         long: '--username USERNAME'

  option :password,
         description: 'RabbitMQ password',
         short: '-p',
         long: '--password PASSWORD'

  option :vhost,
         description: 'RabbitMQ vhost',
         short: '-v',
         long: '--vhost VHOST'

  def run
    rabbitmq_uri = 'amqp://'
    if config[:username]
      rabbitmq_uri << Shellwords.escape(config[:username])
      if config[:password]
        rabbitmq_uri << ':' + Shellwords.escape(config[:password])
      end
      rabbitmq_uri << '@'
    end
    rabbitmq_uri << Shellwords.escape(config[:host]) + '/'
    if config[:vhost]
      rabbitmq_uri << Shellwords.escape(config[:vhost])
    end

    path = Shellwords.escape(config[:path])
    cmd = path + '/python ' + path + '/celery -b ' + rabbitmq_uri
    cmd << ' status --no-color | awk "/`hostname`/ && /OK/ {print $0}" | wc -l'

    if Integer(`#{cmd}`) < 1
      critical('celeryd is not alive')
    else
      ok('celeryd is alive')
    end
  end
end

#!/usr/bin/env ruby
#
# Postgres Stat BGWriter Metrics
# ===
#
# Dependencies
# -----------
# - Ruby gem `pg`
#
#
# Copyright 2012 Kwarter, Inc <platforms@kwarter.com>
# Author Gilles Devaux <gilles.devaux@gmail.com>
#
# Released under the same terms as Sensu (the MIT license); see LICENSE
# for details.


require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/metric/cli'
require 'socket'

require_relative 'postgres-wrap-psql.rb'

class PostgresStatsDBMetrics < Sensu::Plugin::Metric::CLI::Graphite
  option :psql,
         description: 'Path to psql binary',
         long: '--psql PATH',
         required: true

  option :hostname,
         description: 'Hostname to login to',
         short: '-h HOST',
         long: '--hostname HOST',
         default: 'localhost'

  option :port,
         description: 'Database port',
         short: '-P PORT',
         long: '--port PORT',
         default: 5432

  option :user,
         description: 'Postgres User',
         short: '-u USER',
         long: '--user USER',
         default: 'postgres'

  option :scheme,
         description: 'Metric naming scheme, text to prepend to $queue_name.$metric',
         long: '--scheme SCHEME',
         default: "#{Socket.gethostname}.postgresql"

  def run
    timestamp = Time.now.to_i

    run_psql(config, "select checkpoints_timed, checkpoints_req, buffers_checkpoint, buffers_clean, maxwritten_clean, buffers_backend, buffers_alloc from pg_stat_bgwriter").each do |result|
      row = result.split('|')

      output "#{config[:scheme].split('.')[0]}.postgresql.bgwriter.checkpoints_timed",  row[0], timestamp
      output "#{config[:scheme].split('.')[0]}.postgresql.bgwriter.checkpoints_req",    row[1], timestamp
      output "#{config[:scheme].split('.')[0]}.postgresql.bgwriter.buffers_checkpoint", row[2], timestamp
      output "#{config[:scheme].split('.')[0]}.postgresql.bgwriter.buffers_clean",      row[3], timestamp
      output "#{config[:scheme].split('.')[0]}.postgresql.bgwriter.maxwritten_clean",   row[4], timestamp
      output "#{config[:scheme].split('.')[0]}.postgresql.bgwriter.buffers_backend",    row[5], timestamp
      output "#{config[:scheme].split('.')[0]}.postgresql.bgwriter.buffers_alloc",      row[6], timestamp
    end

    ok
  end
end

#! /usr/bin/env ruby
#
#   handler-logstash
#
# DESCRIPTION:
#   Designed to take sensu events, transform them into logstah JSON events
#   and ship them to a redis server for logstash to index.  This also
#   generates a tag with either 'sensu-ALERT' or 'sensu-RECOVERY' so that
#   searching inside of logstash can be a little easier.
#
# CUSTOMIZED FOR PARCC HAL - Add fields for HAL in logstash event & UDP event fixes
#
# OUTPUT:
#   plain text
#
# PLATFORMS:
#   Linux
#
# DEPENDENCIES:
#   gem: sensu-plugin
#   gem: diplomat
#
# USAGE:
#
# NOTES:
#   Heavily inspried (er, copied from) the GELF Handler written by
#   Joe Miller.
#
# LICENSE:
#   Zach Dunn @SillySophist http://github.com/zadunn
#   Released under the same terms as Sensu (the MIT license); see LICENSE
#   for details.
#

require 'sensu-handler'
require 'redis'
require 'json'
require 'socket'
require 'time'
require 'logstash-logger'

#
# Logstash Handler
#
class LogstashHandler < Sensu::Handler
  option :hal_event,
         short: '-h',
         long: '--hal',
         description: "Enable/Disable HAL Event",
         boolean: true,
         default: false

  option :HAL_CODE,
         short: '-c HAL_CODE',
         long: '--code HAL_CODE',
         description: "HAL Code mapping for event",
         default: '665'


  def event_name
    @event['client']['name'] + '/' + @event['check']['name']
  end

  def action_to_string
    @event['action'].eql?('resolve') ? 'RESOLVE' : 'ALERT'
  end

  def event_status
    case @event['check']['status']
    when 0
      'OK'
    when 1
      'WARNING'
    when 2
      'CRITICAL'
    else
      'unknown'
    end
  end

  def handle # rubocop:disable all
    time = Time.now.utc.iso8601
    logstash_msg = {
      :@timestamp    => time, # rubocop:disable Style/HashSyntax
      :@version      => 1, # rubocop:disable Style/HashSyntax
      :source        => ::Socket.gethostname, # rubocop:disable Style/HashSyntax
      :tags          => ["sensu-#{action_to_string}"], # rubocop:disable Style/HashSyntax
      :message       => @event['check']['output'], # rubocop:disable Style/HashSyntax
      :host          => @event['client']['name'], # rubocop:disable Style/HashSyntax
      :timestamp     => @event['check']['issued'], # rubocop:disable Style/HashSyntax
      :address       => @event['client']['address'], # rubocop:disable Style/HashSyntax
      :check_name    => @event['check']['name'], # rubocop:disable Style/HashSyntax
      :command       => @event['check']['command'], # rubocop:disable Style/HashSyntax
      :status        => event_status, # rubocop:disable Style/HashSyntax
      :flapping      => @event['check']['flapping'], # rubocop:disable Style/HashSyntax
      :occurrences   => @event['occurrences'], # rubocop:disable Style/HashSyntax
      :action        => @event['action'], # rubocop:disable Style/HashSyntax
    }
    logstash_msg[:type] = settings['logstash']['type'] if settings['logstash'].key?('type')

    if config[:hal_event]
      logstash_msg[:tags] = logstash_msg[:tags] + ['HAL']
      logstash_msg[:HAL_CODE] = config[:HAL_CODE] if config[:HAL_CODE]
    end

    case settings['logstash']['output']
    when 'redis'
      redis = Redis.new(host: settings['logstash']['server'], port: settings['logstash']['port'])
      redis.lpush(settings['logstash']['list'], logstash_msg.to_json)
    when 'udp'
      udp_logger = LogStashLogger.new(type: :udp, host: settings['logstash']['server'], port: settings['logstash']['port'])
      udp_logger.error(logstash_msg)
    end
  end
end

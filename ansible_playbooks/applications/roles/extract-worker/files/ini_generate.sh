#!/bin/bash

source /opt/virtualenv/edextract/bin/activate
cd /opt/edware/conf
python generate_ini.py -e $1 -i settings.yaml -o /opt/edware/conf/smarter.ini

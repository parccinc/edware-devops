#!/bin/bash
# $1 is schema name
# $2 is host
# $3 is password
# $4 is (choose one:) teardown or setup
source /opt/virtualenv/edmigrate/bin/activate
cd /opt/virtualenv/edmigrate/lib/python3.3/site-packages/edschema-0.1-py3.3.egg/edschema

python3.3 -m metadata_generator --metadata cds -s $1 -d edware --host=$2 -u $3 -p $4 -a $5

#!/bin/bash

source /opt/virtualenv/edsftp/bin/activate
cd /opt/virtualenv/edsftp
sftp_driver.py -s -t $1
sftp_driver.py -a -u $2 -t $1 -r sftparrivals

#!/bin/bash

source /opt/virtualenv/hpz/bin/activate
cd /opt/edware/conf
python generate_ini.py -e $1 -o /opt/edware/conf/hpz.ini -p hpz

source /opt/virtualenv/hpz/bin/activate
cd /opt/virtualenv/hpz/lib/python3.3/site-packages/hpz-0.1-py3.3.egg/hpz

python3.3 -m database.metadata_generator --metadata hpz -s hpz -d hpz --host=$1:5432 -u $2 -p $3 -a $4
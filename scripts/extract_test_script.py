from edextract.celery import setup_celery as setup_extract_celery, PREFIX as edextractPrefix
from edcore.utils.utils import read_ini, get_config_from_ini
from edextract.tasks.extract import prepare_path, generate_extract_file, archive
from celery.canvas import chain
from edextract.tasks.constants import Constants as TaskConstants, ExtractionDataType, QueryType
from edextract.tasks.copy_to_sftp_lz import copy_to_sftp_lz

settings = read_ini('/opt/edware/conf/smarter.ini')
sftp_conf = get_config_from_ini(config=settings, config_prefix='extract.')
pickup_conf = get_config_from_ini(config=settings, config_prefix='pickup.')
setup_extract_celery(settings, prefix=edextractPrefix)
sftp_info = [pickup_conf['sftp.hostname'], pickup_conf['sftp.user'], pickup_conf['sftp.private_key_file']]

task = {}
task[TaskConstants.TASK_TASK_ID] = "TASK_ID"
task[TaskConstants.TASK_QUERIES] = {QueryType.QUERY: "SELECT * FROM edware_es_1_7.fact_asmt_outcome"}
task[TaskConstants.EXTRACTION_DATA_TYPE] = ExtractionDataType.QUERY_CSV
task[TaskConstants.TASK_FILE_NAME] = "/opt/edware/extraction/test1.csv"
task[TaskConstants.DIRECTORY_TO_ARCHIVE] = "/opt/edware/extraction/test1.csv"
print("Triggering Extract")
chain(prepare_path.subtask(args=["REQUEST_ID", ["/opt/edware/extraction"]], queue="extract", immutable=True),
      generate_extract_file.subtask(args=["cat", "REQUEST_ID", task], queue="extract", immutable=True),
      archive.subtask(args=["REQUEST_ID", "/opt/edware/extraction/cat_test.zip", "/opt/edware/extraction"], queue="extract", immutable=True),
      copy_to_sftp_lz.subtask(args=["REQUEST_ID", "/opt/edware/extraction/cat_test.zip", "cat", "test_pz_user", sftp_info], queue="extract", immutable=True)).apply_async()
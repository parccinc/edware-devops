#!/bin/bash

DEV_ACCT_NUM=581313647781
QA_ACCT_NUM=540736820077
STG_ACCT_NUM=680003042501
PROD_ACCT_NUM=981328657324

DEV_VPC_ID="vpc-9f60b3fa"
QA_VPC_ID="vpc-0af2706f"
STG_VPC_ID="vpc-9fb33ffa"
PROD_VPC_ID="vpc-717ac015"

CURR_VPC_ID=`aws ec2 describe-vpcs --query "Vpcs[0].VpcId" | sed -e 's/\"//g'`

if [ "$CURR_VPC_ID" == "$DEV_VPC_ID" ] ; then
    aws_current_acct_num="$DEV_ACCT_NUM"
    aws_promoteTo_acct_num="$QA_ACCT_NUM"
    aws_current_profile="dev"
    aws_promoteTo_profile="qa"
elif [ "$CURR_VPC_ID" == "$QA_VPC_ID" ] ; then 
    aws_current_acct_num="$QA_ACCT_NUM"
    aws_promoteTo_acct_num="$STG_ACCT_NUM"
    aws_current_profile="qa"
    aws_promoteTo_profile="stg"
elif [ "$CURR_VPC_ID" == "$STG_VPC_ID" ] ; then
    aws_current_acct_num="$STG_ACCT_NUM"
    aws_promoteTo_acct_num="$PROD_ACCT_NUM"
    aws_current_profile="stg"
    aws_promoteTo_profile="prod"
else
    echo "invalid  or unknown environment entirely....please check with OPs ....exitting now"
    exit 1
fi

AMI_ID_FOR_PROMOTION="$1"

if [[ -z "$AMI_ID_FOR_PROMOTION" ]] ; then
     echo "WARNING!!   the first argument passed in must be the AMI Id with the complete string including the 'ami-' prefix"
     echo "....please re-run this script with proper formed arguments passed in..... exitting now."
     exit 1
fi

# First revoke higher environment's access to ami with this function tag
FUNCTION=$(aws --profile "$aws_current_profile" ec2 describe-tags --filters "Name=resource-id,Values=$AMI_ID_FOR_PROMOTION" "Name=tag-key,Values=Function" --query "Tags[0].Value")
AMIS_TO_REVOKE=$(aws --profile "$aws_promoteTo_profile" ec2 describe-images --owners "$aws_current_acct_num" --filters "Name=tag-value, Values=$FUNCTION" --query "Images[*].ImageId" --output text)

for ami_id in $AMIS_TO_REVOKE
do
    echo "Revoking acces for $ami_id"
    aws ec2 modify-image-attribute --image-id "$ami_id" --launch-permission "{\"Remove\": [{\"UserId\":\"$aws_promoteTo_acct_num\"}]}"
done

# Grant higher environment access to this ami
echo "Giving $aws_promoteTo_profile access to $AMI_ID_FOR_PROMOTION"
aws ec2 modify-image-attribute --image-id "$AMI_ID_FOR_PROMOTION" --launch-permission "{\"Add\": [{\"UserId\":\"$aws_promoteTo_acct_num\"}]}"

echo "Retrieving tags from $aws_current_profile $AMI_ID_FOR_PROMOTION"
TAGS=$(aws --profile "$aws_current_profile" ec2 describe-tags --filters "Name=resource-id,Values=$AMI_ID_FOR_PROMOTION" --query "Tags[*].{Value:Value,Key:Key}")

echo "Adding tags to $aws_promoteTo_profile $AMI_ID_FOR_PROMOTION"
aws --profile "$aws_promoteTo_profile" ec2 create-tags --resources "$AMI_ID_FOR_PROMOTION" --tags "$TAGS"

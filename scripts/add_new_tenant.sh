#!/bin/bash

#
# TODO: Convert this script to a template for generation
#

if [ "$1" = "?" ]; then
  echo "Usage: ./add_new_tenant.sh realm=(realm) tenant_name=(tenant_name) lz_tenant_user=(lz_tenant_user) tenant_state_code=(XX) new_udl_success_code=(XXX) new_udl_failure_code=(XXX) \
  edudl2_version=(edudl2_version) edsftp_version=(edsftp_version) edmigrate_version=(edmigrate_version) edextract_version=(edextract_version) \
  reporting_version=(reporting_version) starmigrate_version=(starmigrate_version) hpz_version=(hpz_version) pentaho_customization_version=(pentaho_customization_version)"
  exit 1
fi

# These arguments are required

for arg in "$@"; do
  pair=(${arg//=/ })
  if [ "${pair[0]}" == "realm" ]; then
    REALM="${pair[1]}"
  fi

  if [ "${pair[0]}" == "tenant_name" ]; then
    TENANT_NAME="${pair[1]}"
  fi

  if [ "${pair[0]}" == "tenant_state_code" ]; then
    TENANT_STATE_CODE="${pair[1]}"
  fi

  if [ "${pair[0]}" == "new_udl_success_code" ]; then
    NEW_UDL_SUCCESS_CODE="${pair[1]}"
  fi

  if [ "${pair[0]}" == "new_udl_failure_code" ]; then
    NEW_UDL_FAILURE_CODE="${pair[1]}"
  fi

  if [ "${pair[0]}" == "edudl2_version" ]; then
    EDUDL2_VERSION="${pair[1]}"
  fi

  if [ "${pair[0]}" == "edsftp_version" ]; then
    EDSFTP_VERSION="${pair[1]}"
  fi

  if [ "${pair[0]}" == "edmigrate_version" ]; then
    EDMIGRATE_VERSION="${pair[1]}"
  fi

  if [ "${pair[0]}" == "edextract_version" ]; then
    EDEXTRACT_VERSION="${pair[1]}"
  fi

  if [ "${pair[0]}" == "reporting_version" ]; then
    REPORTING_VERSION="${pair[1]}"
  fi

  if [ "${pair[0]}" == "starmigrate_version" ]; then
    STARMIGRATE_VERSION="${pair[1]}"
  fi

  if [ "${pair[0]}" == "hpz_version" ]; then
    HPZ_VERSION="${pair[1]}"
  fi

  if [ "${pair[0]}" == "pentaho_customization_version" ]; then
    PENTAHO_CUSTOMIZATION_VERSION="${pair[1]}"
  fi

  if [ "${pair[0]}" == "lz_tenant_user" ]; then
    LZ_TENANT_USER="${pair[1]}"
  fi

  unset pair
done

# A variable declaration block

RELEASE="pentaho"
STG_REALM="test-stg-env"
PROD_REALM="test-prod-env"

# Check the environment type

HOST_IP=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)

if [[ $HOST_IP == "10.172."* ]]; then
  ENVIRONMENT="dev"
elif [[ "$HOST_IP" == "10.173."* ]]; then
  ENVIRONMENT="qa"
elif [[ "$HOST_IP" == "10.175."* ]]; then
  ENVIRONMENT="stg"
else
  ENVIRONMENT="prod"
fi

# Check the orchestrator instance id

INSTANCE_ID=$(curl http://169.254.169.254/latest/meta-data/instance-id)

# Set a path to the private ssh key

case $ENVIRONMENT in
  dev )
    PRIVATE_KEY="~/.ssh/amplify_parcc_ci_adminkeys.pem" ;;
  qa )
    PRIVATE_KEY="~/.ssh/amplify_parcc_qa_ci_adminkeys.pem" ;;
  stg )
    PRIVATE_KEY="~/.ssh/stg-$RELEASE-$INSTANCE_ID.pem" ;;
  prod )
    PRIVATE_KEY="~/.ssh/prod-$RELEASE-$INSTANCE_ID.pem" ;;
esac

# Set a path to the vaulted_var_file

case $ENVIRONMENT in
  dev )
    VAULTED_VAR_FILE="../vault_vars/dev_passwords.yml" ;;
  qa )
    VAULTED_VAR_FILE="../vault_vars/qa_passwords.yml" ;;
  stg )
    VAULTED_VAR_FILE="../vault_vars/${STG_REALM}_passwords.yml" ;;
  prod )
    VAULTED_VAR_FILE="../vault_vars/${PROD_REALM}_passwords.yml" ;;
esac

# Change a working directory

cd ../ansible_playbooks

# Execute playbooks in order

ansible-playbook -i infrastructure/inventory/$ENVIRONMENT infrastructure/add-new-tenant-infrastructure.yml -e realm=$REALM -e make_elbs=true \
  -e tenant_name=$TENANT_NAME -f 30 --private-key=$PRIVATE_KEY

ansible-playbook -i applications/inventory/ec2.sh applications/genvars.yml -e realm=$REALM -e tenant_name=$TENANT_NAME \
  -e tenant_state_code=$TENANT_STATE_CODE -e new_udl_success_code=$NEW_UDL_SUCCESS_CODE -e new_udl_failure_code=$NEW_UDL_FAILURE_CODE \
  -e vaulted_var_file=$VAULTED_VAR_FILE -e elb_present=true --private-key=$PRIVATE_KEY

ansible-playbook -i applications/inventory/ec2.sh applications/add-new-tenant-applications.yml -e realm=$REALM -e tenant=$TENANT_NAME \
  -e new_udl_success_code=$NEW_UDL_SUCCESS_CODE -e new_udl_failure_code=$NEW_UDL_FAILURE_CODE \
  -e tenant_name=$TENANT_NAME -e edudl2_version=$EDUDL2_VERSION -e edsftp_version=$EDSFTP_VERSION \
  -e edmigrate_version=$EDMIGRATE_VERSION -e edextract_version=$EDEXTRACT_VERSION \
  -e reporting_version=$REPORTING_VERSION -e starmigrate_version=$STARMIGRATE_VERSION -e hpz_version=$HPZ_VERSION \
  -e pentaho_customization_version=$PENTAHO_CUSTOMIZATION_VERSION -e reinitialize_tenant_dbs=true \
  -e tenant_user=$LZ_TENANT_USER -e vaulted_var_file=$VAULTED_VAR_FILE -e elb_present=true \
  --limit=$TENANT_NAME:parcc-reporting:udl-lz:udl-queue:udl-util:udl-queue:udl-app:migrate-queue:starmigrate-queue:extract-queue:cds-queue:pentaho:hpz-app-server:reporting-cache-warmer:loghost:rds:extract-worker:pdf-worker \
  --private-key=$PRIVATE_KEY

#!/bin/bash

# Requirements:
# Options:
# 	BUCKET: 
#		Usage:	 BUCKET=<S3 Bucket name>
#		Desc:  	 Bucket to push to. 
# 	SRC_FOLDER:  
#		Usage:	 FOLDER=<Folder name>
#		Desc:  	 Folder to pull from. 	
#		Default: Default current directory
# Eg Usage:
#    	sh parcc_devops-push_artifacts_to_s3.sh BUCKET=<BUCKET NAME>
#    		This will upload the contents of current folder to the provided bucket
#    	sh parcc_devops-push_artifacts_to_s3.sh BUCKET=<BUCKET NAME> SRC_FOLDER=<Source folder>
#    		This will upload the contents of provided source folder to the provided bucket

PTS_SRC_FOLDER="."
PTS_BUCKET=""
PTS_S3COMMAND="sync"

for arg in "$@"
do
	pair=(${arg//=/ })
	
	if [ "${pair[0]}" == "SRC_FOLDER" ] ; then
		PTS_SRC_FOLDER="${pair[1]}"
	fi

	if [ "${pair[0]}" == "BUCKET" ] ; then
		PTS_BUCKET="${pair[1]}"
	fi
done

if [ "$PTS_BUCKET" == "" ]; then
	echo "Required option bucket is not provided."
	echo "ex usage: sh parcc_devops-push_artifacts_to_s3.sh BUCKET=<BUCKET NAME>"
	echo "   This will upload the contents of the current folder to the provided bucket"
else 
	PTS_BUCKET_FOLDER=$(date -u +%Y-%m-%d_%H-%M-%S)

	aws s3 sync $PTS_SRC_FOLDER s3://$PTS_BUCKET/$PTS_BUCKET_FOLDER

	echo "Artifacts from $PTS_SRC_FOLDER pushed to s3 bucket $PTS_BUCKET/$PTS_BUCKET_FOLDER"

	unset PTS_SRC_FOLDER
	unset PTS_BUCKET
	unset PTS_S3COMMAND
	unset PTS_BUCKET_FOLDER
fi

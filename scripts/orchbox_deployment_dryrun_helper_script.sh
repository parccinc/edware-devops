#!/bin/bash


CURRDIR=$(dirname $0)
ANSIBLE_DEVOPS_HOME=$CURRDIR/../
cd $ANSIBLE_DEVOPS_HOME

export AWS_DEFAULT_PROFILE=qa
export AWS_PROFILE=qa

# from the current  edware_devops local git dirpath as parent to this script's scm location, now do the following...
tar -c --exclude=".git/" --exclude=".DS_Store" -f devops-testing.tar ansible_playbooks/ scripts/ 

aws s3 cp ./devops-testing.tar s3://amplify-release-artifacts-qa-gamma/testing_v3/devops-test3.tar


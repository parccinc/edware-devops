#!/bin/bash

DATE_SUFFIX=`/bin/date +%Y%m%d%H%M%S`
ec2ID_to_export="$1"
#ec2ID_to_export=i-ce3aa09c
AWS_ACCNT_OWNER_ID=581313647781
new_snap_of_gitorious_ec2_name="gitorious_backup-snapshot_ami-$DATE_SUFFIX"

#  Util function to remove the old AMI and Snapshot resources with a limit of Keeping the 5 MOST RECENT snaps/amis
function cleanup_older_backup_ami_and_snaps() {
    LIST_OF_GITORIOUS_BACKUP_SNAPSHOT_AMI_NAMES=`aws ec2 describe-images --owners "$AWS_ACCNT_OWNER_ID" --filters "Name=tag-value,Values=gitorious_backup*" | grep -o -E "Name\": *\".*snapshot_ami-[a-z0-9]*" | sed -e "s/Name\"\: *\"//" | sort -r -d`
    ami_counter=0
    for ami_name in $LIST_OF_GITORIOUS_BACKUP_SNAPSHOT_AMI_NAMES
    do
        (( ami_counter++ ))
        if [ $ami_counter -gt 4 ] ; then
            MATCHED_AMI_INFO_FULL=`aws ec2 describe-images --owners "$AWS_ACCNT_OWNER_ID" --filters Name=name,Values="$ami_name"`
            MATCHED_AMI_ID=`echo "$MATCHED_AMI_INFO_FULL" | grep -o -E "ImageId\": *\"ami-[a-z0-9]*" | sed -e "s/ImageId\"\: *\"//"`
            MATCHED_SNAPSHOT_ID=`echo "$MATCHED_AMI_INFO_FULL" | grep -o -E "SnapshotId\": *\"snap-[a-z0-9]*" | sed -e "s/SnapshotId\"\: *\"//"`
            
            aws ec2 deregister-image --image-id $MATCHED_AMI_ID
            aws ec2 delete-snapshot --snapshot-id $MATCHED_SNAPSHOT_ID
        fi
    done
}

##
# Main Script Processing
###
if [ "$ec2ID_to_export" != "" ] ; then
    # Make the AMI and Snapshot of our Gitorious Server with codebase on root volume 
    #  ....and output the AMI_ID for use as a variable value to other actions on it
    NEW_AMI_ID=`aws ec2 create-image --description "clone of PARCC-AWS-gitorious dev instance for Amplify-PARCC-devel_use" --instance-id "$ec2ID_to_export" --name "$new_snap_of_gitorious_ec2_name" --no-reboot --output text`
    # Grab the Snapshot ID specifically (b/c  it is not returned by the previous image creation output text, but we want to tag it too)
    NEW_SNAPSHOT_ID=`aws ec2 describe-images --image-ids "$NEW_AMI_ID" --query 'Images[0].BlockDeviceMappings[0].Ebs.SnapshotId' | sed -e "s/\"//g"`
    # Now, tag the resources that we just produced from the Gitorious Backup run
    aws ec2 create-tags --resources $NEW_AMI_ID $NEW_SNAPSHOT_ID --tags Key=Name,Value="$new_snap_of_gitorious_ec2_name" Key=backup_timestamp,Value="$DATE_SUFFIX"
    
    # and lastly, we remove the old AMI and Snapshot resources with a limit of Keeping the 5 MOST RECENT snaps/amis
    cleanup_older_backup_ami_and_snaps
else
    echo "WARNING!! you must provide a valid ec2 instance ID string as an argument when calling this script! ....please try again with that value. Exitting now"
    exit 1
fi

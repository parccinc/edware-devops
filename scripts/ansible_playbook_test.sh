#!/bin/bash

if [ "$#" == "10" ]
then
    VERBOSE=$1
    shift
fi

if [ "$#" != "9" ]; then
	echo "Usage: sh ansible-playbook_test.sh (realm) (edudl2version) (edsftpversion) (edmigrateversion) (edextractversion) (starmigrateversion) (reportingversion) (hpzversion) (pentahocustomizationversion)"
	exit 1
fi

export REALM=$1

cd $WORKSPACE/ansible_playbooks

./infrastructure/parallel-infrastructure-playbook.py $1 dev \
                 -e make_elbs=true -e '{"web_groups": ["{{web_group_from_within_vpc}}", "{{web_group_from_amplify}}"]}' -e '{"pentaho_groups": "{{ web_groups + [pentaho_group] }}"}' \
                 -e attach_backup_volume=true \
                 $VERBOSE

if [ "$?" = "0" ]
then
    ./applications/parallel-application-playbook.py $1 \
                     -e edudl2_version=$2 \
                     -e edsftp_version=$3 \
                     -e edmigrate_version=$4 \
                     -e edextract_version=$5 \
                     -e starmigrate_version=$6 \
                     -e reporting_version=$7 \
                     -e hpz_version=$8 \
                     -e pentaho_customization_version=$9 \
                     -e reporting_server_address="{{realm}}-reporting-elb{{hosted_zone}}" \
                     -e override_auth_policy_secure="True" -e hpz_frs_port=8080 \
                     -e override_reporting_auth_saml_issuer_name="https://{{reporting_server_address}}/sp.xml" \
                     -e override_analytics_url="https://{{pentaho_server_address}}/pentaho" \
                     -e override_hpz_file_registration_url="http://{{ hpz_app_server_pvtdns_hostname }}:{{ hpz_frs_port }}/registration" \
                     -e override_hpz_auth_saml_issuer_name="https://{{hpz_server_address}}/sp.xml" \
                     -e override_hpz_file_upload_base_url="http://{{ hpz_app_server_pvtdns_hostname }}:{{ hpz_frs_port }}/files" \
                     -e override_hpz_frs_download_base_url="https://{{hpz_server_address}}/download" \
                     -e elb_present=true -e url_protocol=https -e web_heartbeat_port=443 \
                     -e pentaho_heartbeat_port=443 -e '{"e2e_monitoring_topics": ["e2e-heartbeats"]}' \
                     -e reinitialize_tenant_dbs=true \
                     -e mount_backup_volume=true \
                     -e vaulted_var_file=../vault_vars/dev_passwords.yml \
                     --private-key=~/.ssh/amplify_parcc_ci_adminkeys.pem \
                     $VERBOSE

    if [ "$?" = "0" ]
    then
        exit $?

    else

        echo "Applications playbook failed"
        ansible-playbook \
                         -f 50 \
                         -i ./infrastructure/inventory/ec2.sh \
                         -e realm=$1 \
                         -e terminate_elbs=true \
                         ./infrastructure/site-terminate.yml $VERBOSE
        exit 1

    fi

else
    echo "Infrastructure playbook failed"
    ansible-playbook \
                     -f 50 \
                     -i ./infrastructure/inventory/ec2.sh \
                     -e realm=$1 \
                     -e terminate_elbs=true \
                     ./infrastructure/site-terminate.yml $VERBOSE
    exit 1

fi

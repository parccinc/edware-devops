#!/bin/bash

# =======
# Note: By default, this script will clean up after itself and delete the local repo container folder
# 
# USAGE:   if you want to keep the checked-out branches on this local machine,
#    pass "--no-self-cleanup" as an arg to the script when calling via terminal
#
# =======

CURR_DIR=(dirname $0)
if [ $# -gt 0 ] && [[ -n `echo "$@" | grep -o "no-self-cleanup"` ]] ; then
    INCLUDE_SELF_CLEANUP_ACTION=N
else
    INCLUDE_SELF_CLEANUP_ACTION=Y
fi
DATE_STR_OF_LOCALRUN=`date +%Y%m%d_%H%M`

##===============
# Constants and Variables
##===============
PROJECT_BASENAME="parcc"
REPO_BASENAME="edware"

REPO_SUFFIX_DEVOPS="devops"
REPO_SUFFIX_DBARCH="db_arch"
REPO_SUFFIX="${REPO_SUFFIX_DEVOPS}"

GITORIOUS_BASE_URL="54.88.146.63"
WGGITHUB_BASE_URL="github.wgenhq.net"
AMPLIFY_STASH_BASE_URL="git.amplify.com"

GITORIOUS_PROJECT_NAME="${PROJECT_BASENAME}"
GITHUB_PROJECT_NAME="${PROJECT_BASENAME}"
AMPLIFY_STASH_PROJECT_NAME="edware-${PROJECT_BASENAME}"

GITORIOUS_REPO_NAME="${REPO_BASENAME}_${REPO_SUFFIX}"
AMPLIFY_STASH_REPO_NAME="${REPO_BASENAME}-${REPO_SUFFIX}"

GITORIOUS_PROJECT_URL_PATH="${GITORIOUS_PROJECT_NAME}/${GITORIOUS_REPO_NAME}"
AMPLIFY_STASH_PROJECT_URL_PATH="${AMPLIFY_STASH_PROJECT_NAME}/${AMPLIFY_STASH_REPO_NAME}"

#AMPLIFY_STASH_PROJECT_URL_PATH="edware-parcc/edware_db_arch"
#GITHUB_PROJECT_URL_PATH="Ed-Ware-SBAC/parcc-edware_devops"

LOCAL_FOLDER_ROOT_FOR_MIRROR_REPOS=$HOME/Desktop/amplify_edware_projects-gitrepo_migration_process-homedir
LOCAL_GITREPO_BASENAME="parcc-edware-${DATE_STR_OF_LOCALRUN}"

##===============
# Main process steps
##===============

if [[ ! -d "$LOCAL_FOLDER_ROOT_FOR_MIRROR_REPOS" ]]; then
    mkdir $LOCAL_FOLDER_ROOT_FOR_MIRROR_REPOS
fi

cd $LOCAL_FOLDER_ROOT_FOR_MIRROR_REPOS
if [[ ! -d "$LOCAL_FOLDER_ROOT_FOR_MIRROR_REPOS/$LOCAL_GITREPO_BASENAME" ]]; then
    git clone git@${GITORIOUS_BASE_URL}:${GITORIOUS_PROJECT_URL_PATH}.git ${LOCAL_GITREPO_BASENAME}    
fi

cd $LOCAL_FOLDER_ROOT_FOR_MIRROR_REPOS/$LOCAL_GITREPO_BASENAME
LIST_ALL_BRANCHES_IN_REPO=`git branch -r`
LIST_OF_BRANCHES_TO_MIRROR=`echo "$LIST_ALL_BRANCHES_IN_REPO" | sed -e "s/origin\///g" | sed -e "s/^\ *//g" | sed -e "s/\ *//g"`

for branchname in ${LIST_OF_BRANCHES_TO_MIRROR}
do
    echo $branchname
    if [ "$branchname" != "master" ] ; then
        git checkout -b "$branchname" "origin/$branchname"
    fi
done

git remote add new-origin git@${AMPLIFY_STASH_BASE_URL}:${AMPLIFY_STASH_PROJECT_URL_PATH}.git
#git remote add new-origin git@${WGGITHUB_BASE_URL}:${GITHUB_PROJECT_URL_PATH}.git

git checkout master

git push --all new-origin
git push --tags new-origin

if [ "$INCLUDE_SELF_CLEANUP_ACTION" == "Y" ] ; then
    cd $HOME/Desktop
    rm -R $LOCAL_FOLDER_ROOT_FOR_MIRROR_REPOS
    echo "Done and removed the local checkout already....thanks!"
else
    echo "Finished all actions....all local copies of gitorious branches have not been cleaned out yet.  Please do clear them before rerunning this script again."
fi



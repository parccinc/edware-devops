#!/bin/bash


THISRUN_DATESUFFIX=`date +%Y%m%d`
# Create a directory for the job definitions
#mkdir -p $BUILD_ID
cd $JENKINS_HOME

# Create an archive from all copied files 
tar -czf $WORKSPACE/jenkins-configuration-${JENKINS_BUILDPATH_TYPE}-alpha.tar.gz --exclude="war"  --exclude="tools" --exclude="workspace" --exclude="jobs/*/builds/archive" *

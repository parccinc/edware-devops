#!/bin/bash

##========================================================
#  [  Usage Instructions  ] :
#
#   Run this script as root or sudo
#
#   Arguments:
#     ec2helper_version: The RPM version of the ec2helper to install (required)
#     orchestrator_realm: The name of the orchestration realm (required)
#     release: Which release to deploy. Ex: gamma, post-uat (required)
#     yum_version: Which S3 yum version to use for the managed realm (required on stg and prod)
#     artifact_folder: Which version of the application specific artifacts to use in a deployment (required on stg and prod)
#
#   Example:
#     ./run_ec2helper_script.sh ec2helper_version=655 orchestrator_realm=test artifact_folder=2015-05-17_00-00-00 yum_version=v1.14 release=pentaho
#
#   [ Note: this script checks for an existing Orchestrator with the provided realm,
#             and will only bring up a new instance if not taken already,
#             Then, runs Orchestrator Appl-Playbook for the matching orchestrator instance
#             (i.e. providing an update or fresh config )  ]
##========================================================
for arg in "$@"
do
    pair=(${arg//=/ })

    if [ "${pair[0]}" == "ec2helper_version" ] ; then
        EC2HELPER_RPM_BUILDNUM="${pair[1]}"
    fi

    if [ "${pair[0]}" == "orchestrator_realm" ] ; then
        TARGET_REALM="${pair[1]}"
    fi

    if [ "${pair[0]}" == "yum_version" ] ; then
        YUM_VERSION="${pair[1]}"
    fi

    if [ "${pair[0]}" == "artifact_folder" ] ; then
        ARTIFACT_FOLDER="${pair[1]}"
    fi

    if [ "${pair[0]}" == "release" ] ; then
        RELEASE="${pair[1]}"
    fi
done

ANSIBLE_DEVOPS_HOME=/opt/edware_devops
AWSCLI_INSTALLDIR=/opt/parcc/integration/aws
EC2HELPER_INSTANCE_PROFILE_ROLENAME=ec2helper

EC2HELPER_RPM_BASENAME="edware-devops-ec2helper"
EC2HELPER_INSTALLED_PACKAGE_SUFFIX=".el6.x86_64"
ARBITRARY_RPMVERSION_LABEL="1.01"

if [[ -z "$EC2HELPER_RPM_BUILDNUM" || -z "$TARGET_REALM" || -z "$RELEASE" ]] ; then
    echo "The EC2Helper RPM Version, the Orchestrator Realm, and the Release need to be specified!"
    exit 1
fi

#
# Modify an s3 yum repository version
#

sed -i s/v[0-9]\.[0-9]*/${YUM_VERSION}/g /etc/yum.repos.d/s3iam.repo
yum clean all

EC2HELPER_RPM_PACKAGE_TO_INSTALL=${EC2HELPER_RPM_BASENAME}-${ARBITRARY_RPMVERSION_LABEL}-${EC2HELPER_RPM_BUILDNUM}${EC2HELPER_INSTALLED_PACKAGE_SUFFIX}

if [[ -z `which aws` ]] ; then
    wget 'https://s3.amazonaws.com/aws-cli/awscli-bundle.zip'
    unzip -d /tmp ./awscli-bundle.zip
    /tmp/awscli-bundle/install -i $AWSCLI_INSTALLDIR -b /usr/local/bin/aws
    rm -rf /tmp/awscli-bundle
    rm awscli-bundle.zip
fi

aws configure set region us-east-1
curl -s "http://169.254.169.254/latest/meta-data/iam/security-credentials/$EC2HELPER_INSTANCE_PROFILE_ROLENAME"

DEV_ACCT_NUM=581313647781
QA_ACCT_NUM=540736820077
STG_ACCT_NUM=680003042501
PROD_ACCT_NUM=981328657324

CURR_ENV_ACCTNUM=$(curl -s "http://169.254.169.254/latest/meta-data/iam/info" | grep -o -e "arn\:aws\:iam\:\:[0-9]*\:instance-profile" | sed -e "s/arn\:aws\:iam\:\://" -e "s/\:instance-profile//")
case $CURR_ENV_ACCTNUM in
    ${DEV_ACCT_NUM} ) CURR_ENVIRONMENT="dev"; SRC_S3_BASENAME="amplify-nightly-artifacts-dev/ec2helper/$RELEASE"; KEYS_S3_BASENAME="amplify-parcc-keys-dev";;
    ${QA_ACCT_NUM} ) CURR_ENVIRONMENT="qa"; SRC_S3_BASENAME="amplify-nightly-artifacts-dev/ec2helper/$RELEASE"; KEYS_S3_BASENAME="amplify-parcc-keys-qa";;
    ${STG_ACCT_NUM} ) CURR_ENVIRONMENT="stg"; SRC_S3_BASENAME="amplify-release-artifacts-qa/ec2helper/$RELEASE"; KEYS_S3_BASENAME="amplify-parcc-keys-stg";;
    ${PROD_ACCT_NUM} ) CURR_ENVIRONMENT="prod"; SRC_S3_BASENAME="amplify-release-artifacts-stg/ec2helper/$RELEASE"; KEYS_S3_BASENAME="parcc-keys-prod";;
    * ) echo "Invalid or unrecognized AWS Account Number of current VPC Settings...exiting now with no further action"; exit 1;;
esac

#
# Install Ansible
#

yum -y install ansible python-boto

if [[ ( ${CURR_ENVIRONMENT} == "stg" || ${CURR_ENVIRONMENT} == "prod" ) && \
      ( -z "${YUM_VERSION}" || -z "${ARTIFACT_FOLDER}" ) ]] ; then
    echo "Yum version and artifact folder need to be specified for this environment."
    exit 1
fi

# Grab pem key from secure S3
if [[ -z `ls -Am ~/.ssh/ | grep -o "amplify_parcc_${CURR_ENVIRONMENT}_ci_adminkeys.pem"` ]] ; then
    aws s3 cp s3://${KEYS_S3_BASENAME}/ansible-user-key/amplify_parcc_${CURR_ENVIRONMENT}_ci_adminkeys.pem ~/.ssh/amplify_parcc_${CURR_ENVIRONMENT}_ci_adminkeys.pem
    chmod 0400 ~/.ssh/amplify_parcc_${CURR_ENVIRONMENT}_ci_adminkeys.pem
fi
# Grab default vault_pass file from secure S3
if [[ -z `ls -Am ~/ | grep -o ".vault_pass.txt"` ]] ; then
    aws s3 cp s3://${KEYS_S3_BASENAME}/ansible-vault/vault_pass.txt ~/.vault_pass.txt
fi

# Grab the specified ec2helper rpm version from s3
TARGET_S3_FILEPATH=$(aws s3 ls s3://"$SRC_S3_BASENAME/" --recursive | grep -o -e "ec2helper\/${RELEASE}\/.*\/edware-devops-ec2helper-1\.01-${EC2HELPER_RPM_BUILDNUM}\.el6\.x86_64\.rpm" | sed -e "s/ec2helper\/"${RELEASE}"\///")
TARGET_S3_FILENAME=$(echo "$TARGET_S3_FILEPATH" | grep -o -e "edware\-devops\-ec2helper\-[0-9]\.[0-9]*\-[0-9]*\.el6\.x86_64\.rpm")
aws s3 cp s3://"$SRC_S3_BASENAME"/"$TARGET_S3_FILEPATH" "/tmp/$TARGET_S3_FILENAME"

# Check to see if an ec2Helper rpm is already installed (not necessarily the "latest" one)
EXISTING_EC2HELPER_RPM_BUILDNUM=$(rpm -qa | grep "edware-devops-ec2helper*")
if [ "$EXISTING_EC2HELPER_RPM_BUILDNUM" == "$EC2HELPER_RPM_PACKAGE_TO_INSTALL" ] ; then
    echo "[NOTICE] specified RPM Version already installed as current ec2Helper RPM Version: '${EXISTING_EC2HELPER_RPM_BUILDNUM}'"
else
    echo "[NOTICE] current ec2Helper RPM Version installed is: '${EXISTING_EC2HELPER_RPM_BUILDNUM}'"
    echo "[NOTICE] new ec2Helper RPM Version to be installed is: '${TARGET_S3_FILENAME}'"
    if [[ ! -e "/tmp/$TARGET_S3_FILENAME" ]] ; then
        echo "WARNING!! no ec2Helper RPM found or unmatched RPM_BUILDNUM was identified.  Please investigate and try again..."
        exit 1
    fi
    rpm -Uvh /tmp/${TARGET_S3_FILENAME}
fi

VERSION_DIRECTORY=${ANSIBLE_DEVOPS_HOME}/ansible_playbooks/applications/vars
VERSION_FILE=${VERSION_DIRECTORY}/${CURR_ENVIRONMENT}_current_release_deployment_vars.yml
mkdir -p ${VERSION_DIRECTORY}
echo "---" > "${VERSION_FILE}"

# Set the version specific deployment file
if [[ $YUM_VERSION && $ARTIFACT_FOLDER ]] ; then
    echo "orchestrator_realm_basename: '${TARGET_REALM}'" >> "${VERSION_FILE}"
    echo "artifact_folder_name_to_release: '${ARTIFACT_FOLDER}'" >> "${VERSION_FILE}"
    echo "specified_yum_version_to_release: '${YUM_VERSION}'" >> "${VERSION_FILE}"
fi

CHECK_TARGET_REALM_ORCH_EXISTS=`aws ec2 describe-instances --filters "Name=tag:realm,Values=$TARGET_REALM" "Name=instance-state-name,Values=pending,running" "Name=tag:type,Values=orchestrator" --query 'Reservations[0].Instances[0].Tags[?Key==\`realm\`].Value' | tr -d '"[] \n'`

cd $ANSIBLE_DEVOPS_HOME/ansible_playbooks/
if [[ $CHECK_TARGET_REALM_ORCH_EXISTS ]] ; then
    ansible-playbook -i ./infrastructure/inventory/${CURR_ENVIRONMENT}_orchestrator -e realm=$TARGET_REALM ./infrastructure/ec2.yml
fi
ansible-playbook -i ./applications/inventory/ec2.sh -e "realm=$TARGET_REALM" --private-key=~/.ssh/amplify_parcc_${CURR_ENVIRONMENT}_ci_adminkeys.pem ./applications/ansible-orchestrator-server.yml

# Upload the ec2helper rpm to release-ready s3 bucket (when applicable)
if [[ "$?" -eq 0 && ${CURR_ENVIRONMENT} != "prod" ]] ; then
    aws s3 cp "/tmp/$TARGET_S3_FILENAME" s3://"amplify-release-artifacts-$CURR_ENVIRONMENT/ec2helper/$RELEASE/$TARGET_S3_FILEPATH"
fi

rm ~/.ssh/amplify_parcc_${CURR_ENVIRONMENT}_ci_adminkeys.pem ~/.vault_pass.txt

echo "EC2Helper process has finished its tasks...."


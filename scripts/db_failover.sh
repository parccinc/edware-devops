#!/bin/bash
cd ../ansible_playbooks

if [ "$1" = "?" ]; then
    echo "Usage: ./failover tenant=dog realm=maks-demo starmigrate_version=0.6-1157.el6.x86_64 env=dev orchestrated_env_realm=realm_name(only for stg and prod env)"
    exit 1
fi

for arg in "$@"
do

  pair=(${arg//=/ })

    if [ "${pair[0]}" == "tenant" ] ; then
        TENANT="${pair[1]}"
    fi

    if [ "${pair[0]}" == "realm" ] ; then
        REALM="${pair[1]}"
    fi

    if [ "${pair[0]}" == "starmigrate_version" ] ; then
        STARMIGRATE="${pair[1]}"
    fi

    if [ "${pair[0]}" == "edmigrate_version" ] ; then
        EDMIGRATE="${pair[1]}"
    fi

    if [ "${pair[0]}" == "env" ] ; then
        ENV="${pair[1]}"
    fi

    if [ "${pair[0]}" == "orchestrated_env_realm" ] ; then
        ORCH_REALM="${pair[1]}"
    fi

    if [ "${pair[0]}" == "database" ] ; then
        DATABASE="${pair[1]}"
    fi

    if [ "${pair[0]}" == "backup_period" ] ; then
        BACKUP_PERIOD="${pair[1]}"
    fi

    if [ "${pair[0]}" == "timestamp" ] ; then
        TIMESTAMP="${pair[1]}"
    fi

    if [ "${pair[0]}" == "year" ] ; then
        YEAR="${pair[1]}"
    fi

  unset pair
done

if [ "$ENV" = "dev" ] || [ "$ENV" = "qa" ] ; then
    VAULT_FILE="${ENV}_passwords.yml"
elif [ "$ENV" = "stg" ] || [ "$ENV" = "prod" ] ; then
    VAULT_FILE="${ORCH_REALM}_passwords.yml"
else
    echo "$ENV environment do not exist, try other one"
    exit 1
fi

#
# Check if a backup volume is required
#

if [[ $DATABASE == "dw" ]]; then
    BACKUP_VOLUME="-e attach_backup_volume=true"
else
    BACKUP_VOLUME=""
fi


#
# Create an ec2 instance if it doesn't exist
#

INSTANCE="${REALM}-${DATABASE}-master-${TENANT}"
STATE=$(./../ansible_playbooks/inventory/ec2.py)

if [[ $STATE != *$INSTANCE* ]]; then
    ansible-playbook -i infrastructure/inventory/$ENV infrastructure/ec2.yml \
    -e realm=$REALM --limit=$DATABASE-master-$TENANT $BACKUP_VOLUME
fi

#
# Run a database failover playbooks
#

case $DATABASE in
    dw )
        ansible-playbook -i applications/inventory/ec2.sh applications/dw-failover.yml \
        -e realm=$REALM -e vaulted_var_file=../vault_vars/$VAULT_FILE -e tenant_name=$TENANT \
        -e backup_period=$BACKUP_PERIOD -e timestamp=$TIMESTAMP -e failover=true \
        -e edmigrate_version=$EDMIGRATE -e mount_backup_volume=true ;;
    cds )
        ansible-playbook -i applications/inventory/ec2.sh applications/cds-failover.yml \
        -e realm=$REALM -e year=$YEAR -e tenant_name=$TENANT -e edmigrate_version=$EDMIGRATE \
        -e vaulted_var_file=../vault_vars/$VAULT_FILE -e failover=true -e reinitialize_tenant_dbs=true ;;
    analytics )
        ansible-playbook -i ./applications/inventory/ec2.sh -e realm=$REALM \
        ./applications/analytics-failover.yml -e tenant_name=$TENANT \
        -e starmigrate_version=$STARMIGRATE -e failover=true -e vaulted_var_file=../vault_vars/$VAULT_FILE ;;
esac

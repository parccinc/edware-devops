#!/bin/bash

if [ -f /root/AWS_bind_refresh.vars ] then
    . /root/AWS_bind_refresh.vars
elif [ -f /opt/itops/bin/AWS_bind_refresh.vars ] then
    . /opt/itops/bin/AWS_bind_refresh.vars
else
        echo "NEED VARS FILE TO BE PRESENT" >&2
        exit 12
fi

TEMPFILECONTENT=`$PYTHONDIR/python $SPORKPATH/AwsSpork/AwsSpork.py -li`
(
flock -x -w $LOCKTO 200 || exit 128
echo "" | /usr/bin/tee $REVZONEFILE > $ZONEFILE
echo ";" | /usr/bin/tee -a $REVZONEFILE >> $ZONEFILE
echo "" | /usr/bin/tee -a $REVZONEFILE >> $ZONEFILE
echo "\$TTL 1h" | /usr/bin/tee -a $REVZONEFILE >> $ZONEFILE
echo "" | /usr/bin/tee -a $REVZONEFILE >> $ZONEFILE
echo "@ IN SOA  ns0.${DOMAIN}. ${PRJ_EMAIL} (" | /usr/bin/tee -a $REVZONEFILE >> $ZONEFILE
echo "          $SERIAL ;serial (version)" | /usr/bin/tee -a $REVZONEFILE >> $ZONEFILE
echo "                  3h      ;refresh period" | /usr/bin/tee -a $REVZONEFILE >> $ZONEFILE
echo "                  1h      ;retry refresh this often" | /usr/bin/tee -a $REVZONEFILE >> $ZONEFILE
echo "                  1w      ;expiration period" | /usr/bin/tee -a $REVZONEFILE >> $ZONEFILE
echo "                  1h      ;Negative caching TTL" | /usr/bin/tee -a $REVZONEFILE >> $ZONEFILE
echo "          )" | /usr/bin/tee -a $REVZONEFILE >> $ZONEFILE
echo "" | /usr/bin/tee -a $REVZONEFILE >> $ZONEFILE
for i in ${NAMESERVERS}
do
        echo "                                         IN NS      ${i}" >> $ZONEFILE
        echo "${i}                                      IN A   10.172.4.10" >> $ZONEFILE
        echo "ns0                                      IN CNAME   ${i}" >> $ZONEFILE
        echo "                                         IN NS   ns1.${DOMAIN}." >> $REVZONEFILE
        #echo "                                         IN NS   ns0.${DOMAIN}." >> $REVZONEFILE
        echo "                                         IN NS   ${i}.${DOMAIN}." >> $REVZONEFILE
        echo "10.4                                     IN PTR  ${i}.${DOMAIN}." >> $REVZONEFILE
        echo "10.4                                     IN PTR  ns0.${DOMAIN}." >> $REVZONEFILE
done


echo "Creating entries..."
while read i
do
        temp=`echo ${i%%'|'*}`
        NEWADDR="`echo ${i} | awk -F '|' '{print $7}' | sed 's/^[^:]*: *//'`"
        LASTOCT="`echo ${NEWADDR##*'.'}`"
        THIRDOCT="`echo ${NEWADDR%'.'*}`"
        THIRDOCT="`echo ${THIRDOCT##*'.'}`"
    NEWHOSTNAME="`echo ${temp##*':'}`"
        NEWHOSTNAME="`echo ${NEWHOSTNAME} | awk -F'.' '{print $1}'`"
        NEWHOSTNAMELEN="`echo ${#NEWHOSTNAME}`"


        echo $NEWADDR | grep "None" > /dev/null
        if [[ $? -eq 0  ]]
        then
          NEWADDR="None"
        fi

set -x
        if [[ $NEWHOSTNAME != *_* ]] && [[ $NEWHOSTNAME != *.* ]] && [[ $NEWHOSTNAME != "None" ]] && [[ $NEWHOSTNAME != *" "* ]] && [[ $NEWADDR != "None" ]] && [[ $NEWHOSTNAMELEN -le 32 ]]
        then
                echo "$NEWHOSTNAME                              IN A $NEWADDR" >> $ZONEFILE
                echo "$LASTOCT.$THIRDOCT                                        IN PTR $NEWHOSTNAME.${DOMAIN}." >> $REVZONEFILE
#               echo "$LASTOCT                                                  IN PTR $NEWHOSTNAME.${DOMAIN}." >> $REVZONEFILE
        fi
set +x
done <<< "$TEMPFILECONTENT"


chown named:named $ZONEFILE
chown named:named $REVZONEFILE

(test ! -s $NAMEDPIDFILE) && echo "named PID file not found!" && exit
echo "Restarting named..."
kill -1 `/bin/cat $NAMEDPIDFILE`
) 200>$ZONEFILE

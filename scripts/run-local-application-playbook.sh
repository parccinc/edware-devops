#!/bin/bash

# Given that an environment exists on aws, this script will run the application playbook in its entirety with the
# latest rpms.

if [ "$#" -lt 1 ]; then
    SCRIPT=`basename "$0"`
    echo "Usage: "${SCRIPT} "<release> <realm> <other arguments to ansible-playbook (e.g. --limit, -f, -v)>"
    exit 1
fi

# Get tagged RPM versions
RELEASE=${1}
shift
TAGGED_RPM_VERSIONS_FILE=$(mktemp /tmp/tagged_rpm_versions.properties.XXXXXX)
aws s3 cp "s3://amplify-parcc-artifacts-${RELEASE}/tagged_rpm_versions.properties" ${TAGGED_RPM_VERSIONS_FILE}
source ${TAGGED_RPM_VERSIONS_FILE}

echo "Running application playbook using versions:"
echo "    edudl2: "${edudl2_version}
echo "    edsftp: "${edsftp_version}
echo "    edmigrate: "${edmigrate_version}
echo "    edextract: "${edextract_version}
echo "    starmigrate: "${starmigrate_version}
echo "    reporting: "${reporting_version}
echo "    hpz: "${hpz_version}
echo "    pentaho_customization_version: "${pentaho_customization_version}

export REALM=${1}
shift

DIR=`dirname "$0"`
pushd $DIR &> /dev/null
../ansible_playbooks/applications/parallel-application-playbook.py ${REALM} \
        -e edudl2_version=${edudl2_version} \
        -e edsftp_version=${edsftp_version} \
        -e edmigrate_version=${edmigrate_version} \
        -e edextract_version=${edextract_version} \
        -e starmigrate_version=${starmigrate_version} \
        -e reporting_version=${reporting_version} \
        -e hpz_version=${hpz_version} \
        -e pentaho_customization_version=${pentaho_customization_version} \
        "$@"
popd &> /dev/null

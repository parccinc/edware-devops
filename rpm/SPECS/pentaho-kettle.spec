%define awsproject parcc
%define project_scmrepo edware-devops
%define ansible_releaseversion 1.8.4

%define vendor Amplify
%define packager Amplify

%define _prefix /opt/pentaho

Name:		pentaho-kettle
Version:	%{ver}
Release:	%{rel}%{?dist}
Summary:	Pentaho Kettle implementation
License:	EULA
Vendor:		%{vendor}
Packager:	%{packager}

URL:		http://www.pentaho.com
Source:		http://downloads.sourceforge.net/project/pentaho/Data%20Integration/5.3/pdi-ce-5.3.0.0-213.zip

BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
Requires:	/usr/sbin/groupadd /usr/sbin/useradd 
Requires: 	java >= 1:1.7.0
BuildArch:	x86_64
AutoReqProv:	no

%description
%{summary}


%prep
%setup -q -n data-integration

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_prefix}

cd %{buildroot}
find . -type f | sed -e 's,^\.,\%attr(-\,root\,root) ,' > %{_tmppath}/package-file.list.%{name}
find . -type l | sed -e 's,^\.,\%attr(-\,root\,root) ,' >> %{_tmppath}/package-file.list.%{name}

%__install -d "%{buildroot}%{_prefix}"
pwd
cd %{_builddir}

cp -pr data-integration "%{buildroot}%{_prefix}"

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,pentaho,pentaho,-)
%doc
%{_prefix}/data-integration

%pre
/usr/sbin/groupadd -r pentaho &>/dev/null || :
/usr/sbin/useradd -g pentaho -s /bin/bash -r -c "pentaho bi" \
-m -d /home/pentaho pentaho &>/dev/null || :

%post

%preun

%postun
if [ "$1" = "0" ]; then
  /usr/sbin/userdel pentaho
  /usr/sbin/groupdel pentaho
fi

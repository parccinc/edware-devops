%define awsproject parcc
%define project_scmrepo edware-devops
%define ansible_releaseversion 1.8.4

%define devops_base_installdir /opt/edware_devops
%define orchestration_deployscripts_rootdir /opt/parcc

%define vendor Amplify
%define packager Amplify

%define _unpackaged_files_terminate_build 0

Name: edware-devops
Summary: Edware Cloud Infrastructure SRCcode package
Version: 1.01
Release: %(echo ${BUILD_NUMBER:="X"})%{?dist}

Group:    Application/SystemTools
License: EULA
Vendor: %{vendor}
Packager: %{packager}

Prefix: /opt/edware_devops
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildArch: x86_64
AutoReqProv: no

%description
Contains packages and libraries used to run ansible-based infrastructure release tasks


%package ec2helper
Summary: The edware-devops Orchestrator-server creation and setup tasks as a subpackage to bootstrap a release process into a promoted environment
Group: Application/SystemTools
%description ec2helper
Subset of devops build-package to provide the means to establish an orchestrator-server to initiate the release process


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{prefix}
cp -r $WORKSPACE/ansible_playbooks %{buildroot}%{prefix}/ansible_playbooks
mkdir %{buildroot}%{prefix}/scripts
cp $WORKSPACE/scripts/add_new_tenant.sh %{buildroot}%{prefix}/scripts/add_new_tenant.sh
cp $WORKSPACE/scripts/db_failover.sh %{buildroot}%{prefix}/scripts/db_failover.sh

cd %{buildroot}
find . -type f | sed -e 's,^\.,\%attr(-\,root\,root) ,' > %{_tmppath}/package-file.list.%{name}
find . -type l | sed -e 's,^\.,\%attr(-\,root\,root) ,' >> %{_tmppath}/package-file.list.%{name}

%files -f %{_tmppath}/package-file.list.%{name}
%defattr(-,root,root)

%files ec2helper
%defattr(-,root,root)
%{prefix}/ansible_playbooks/ansible.cfg
%{prefix}/ansible_playbooks/inventory/ec2.ini
%{prefix}/ansible_playbooks/inventory/ec2.py
%{prefix}/ansible_playbooks/infrastructure/inventory/ec2.sh
%{prefix}/ansible_playbooks/infrastructure/ec2.yml
%{prefix}/ansible_playbooks/infrastructure/ec2-terminate.yml
%{prefix}/ansible_playbooks/infrastructure/elasticache.yml
%{prefix}/ansible_playbooks/infrastructure/elasticache-terminate.yml
%{prefix}/ansible_playbooks/infrastructure/elb.yml
%{prefix}/ansible_playbooks/infrastructure/elb-terminate.yml
%{prefix}/ansible_playbooks/infrastructure/rds.yml
%{prefix}/ansible_playbooks/infrastructure/rds-terminate.yml
%{prefix}/ansible_playbooks/infrastructure/site.yml
%{prefix}/ansible_playbooks/infrastructure/site-terminate.yml
%{prefix}/ansible_playbooks/infrastructure/tasks/add-route53-ec2-instances.yml
%{prefix}/ansible_playbooks/infrastructure/inventory/dev_orchestrator
%{prefix}/ansible_playbooks/infrastructure/inventory/qa_orchestrator
%{prefix}/ansible_playbooks/infrastructure/inventory/stg_orchestrator
%{prefix}/ansible_playbooks/infrastructure/inventory/group_vars/all
%{prefix}/ansible_playbooks/infrastructure/inventory/group_vars/ansible-servers
%{prefix}/ansible_playbooks/infrastructure/inventory/group_vars/dev
%{prefix}/ansible_playbooks/infrastructure/inventory/group_vars/qa
%{prefix}/ansible_playbooks/infrastructure/inventory/group_vars/stg
%{prefix}/ansible_playbooks/infrastructure/roles/ec2/tasks/main.yml
%{prefix}/ansible_playbooks/infrastructure/roles/ec2/tasks/launch_instance.yml
%{prefix}/ansible_playbooks/infrastructure/roles/ec2/tasks/elastic_ip.yml
%{prefix}/ansible_playbooks/infrastructure/roles/add_ebs_vol/tasks/main.yml
%{prefix}/ansible_playbooks/infrastructure/roles/elcache/tasks/main.yml
%{prefix}/ansible_playbooks/infrastructure/roles/elcache_gather_info/tasks/main.yml
%{prefix}/ansible_playbooks/infrastructure/roles/rds/tasks/main.yml
%{prefix}/ansible_playbooks/infrastructure/roles/route53_wrr/tasks/main.yml
%{prefix}/ansible_playbooks/infrastructure/tasks/install-awscli-tool.yml
%{prefix}/ansible_playbooks/infrastructure/tasks/register_elb_dns_name.yml
%{prefix}/ansible_playbooks/applications/ansible-orchestrator-server.yml
%{prefix}/ansible_playbooks/applications/files/aws_s3_shellscripts/pull_artifacts_from_s3.sh
%{prefix}/ansible_playbooks/applications/files/aws_s3_shellscripts/pull_from_lower_s3.sh
%{prefix}/ansible_playbooks/applications/files/deployment_helper_files/raw_vaultvars_template.txt
%{prefix}/ansible_playbooks/applications/files/yum_s3iam_plugin_prereq/s3iam.conf
%{prefix}/ansible_playbooks/applications/files/yum_s3iam_plugin_prereq/s3iam.py
%{prefix}/ansible_playbooks/applications/files/sudoers_orchestrator
%{prefix}/ansible_playbooks/applications/inventory/ec2.sh
%{prefix}/ansible_playbooks/applications/inventory/group_vars/all
%{prefix}/ansible_playbooks/applications/inventory/group_vars/ansible-servers
%{prefix}/ansible_playbooks/applications/inventory/group_vars/dev
%{prefix}/ansible_playbooks/applications/inventory/group_vars/qa
%{prefix}/ansible_playbooks/applications/inventory/group_vars/stg
%{prefix}/ansible_playbooks/applications/pre-validation.yml
%{prefix}/ansible_playbooks/applications/prep_generate_ini_for_tests.yml
%{prefix}/ansible_playbooks/applications/tasks/install-ansible-on_host-task.yml
%{prefix}/ansible_playbooks/applications/tasks/install-awscli-tool.yml
%{prefix}/ansible_playbooks/applications/tasks/parcc_s3yum_plugin_prerequisite_task.yml
%{prefix}/ansible_playbooks/applications/tasks/setup-test-dependencies.yml
%{prefix}/ansible_playbooks/applications/tasks/release_info_properties_deployment.yml
%{prefix}/ansible_playbooks/applications/tasks/yum-install.yml
%{prefix}/ansible_playbooks/applications/tasks/default_sudoers.yml
%{prefix}/ansible_playbooks/applications/roles/common/tasks/setup-users.yml
%{prefix}/ansible_playbooks/applications/templates/sudoers.j2
%{prefix}/ansible_playbooks/applications/templates/a_user_cred.j2
%{prefix}/ansible_playbooks/applications/templates/a_user_conf.j2
%{prefix}/ansible_playbooks/applications/templates/a_user_bashrc.j2
%{prefix}/ansible_playbooks/applications/templates/crontab_scripts/deployment_script_cron.j2
%{prefix}/ansible_playbooks/applications/templates/yum_s3iam_plugin_prereq/amplify_parcc-s3yum.repo.j2
%{prefix}/ansible_playbooks/applications/templates/crontab_scripts/database_backup_script_cron.j2
%{prefix}/ansible_playbooks/applications/templates/deployment_scripts/orchestration_deployment_script.sh.j2
%{prefix}/ansible_playbooks/applications/templates/deployment_scripts/prepare-raw-vaultfile-for-realm.yml.j2
%{prefix}/ansible_playbooks/applications/templates/deployment_scripts/release_specific_deployment_vars.j2
%{prefix}/ansible_playbooks/applications/templates/deployment_scripts/promote_artifacts.sh.j2
%{prefix}/ansible_playbooks/applications/templates/generate_ini_for_tests_scripts/generate_ini_for_tests.sh.j2
%{prefix}/ansible_playbooks/applications/templates/yum_s3_scripts/yum_bucket_update_by_version.sh.j2
%{prefix}/ansible_playbooks/applications/update_release_info_for_orchestrator_deployment.yml
%{prefix}/ansible_playbooks/vault_vars/qa_passwords.yml
%{prefix}/ansible_playbooks/vault_vars/credentials-a_user.yml
%{prefix}/ansible_playbooks/filter_plugins/__init__.py
%{prefix}/ansible_playbooks/filter_plugins/filters.py


%clean
[[ -d /var/lib/jenkins/rpmbuild/ ]] && cp  /var/lib/jenkins/rpmbuild/RPMS/x86_64/edware-devops*.rpm $WORKSPACE/
rm -rf %{buildroot}
rm -rf ${RPM_BUILD_ROOT}

%pre

%post

%preun

%postun


%changelog

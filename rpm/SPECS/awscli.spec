Name: awscli-bundle
Version:	0.1
Release:	1
Summary: Packaged version of the Amazon WebServices Command Lines Tools

Group: Virtualization
License:	EULA
Vendor: Amazon
Packager: Custom
Prefix: /opt/parcc/integration/src/awscli
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildArch: noarch

%description
The Command Line Tool serves as the client interface to the Amazon web service.

%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{prefix}
wget -O /tmp/awscli-bundle.zip 'https://s3.amazonaws.com/aws-cli/awscli-bundle.zip'
unzip -d %{buildroot}%{prefix} /tmp/awscli-bundle.zip
rm /tmp/awscli-bundle.zip
AWSCLI_SRC_VERSION=$(ls -Am %{buildroot}%{prefix}/packages/ | grep -o -e "awscli.*\.tar\.gz" | sed -e "s/awscli-//" | sed -e "s/\.tar\.gz//") 
echo "$AWSCLI_SRC_VERSION" > %{buildroot}%{prefix}/VERSION

cd %{buildroot}
find . -type f | sed -e 's,^\.,\%attr(-\,root\,root) ,' > %{_tmppath}/package-file.list.%{name}
find . -type l | sed -e 's,^\.,\%attr(-\,root\,root) ,' >> %{_tmppath}/package-file.list.%{name}

%files -f %{_tmppath}/package-file.list.%{name}
%defattr(-,root,root)

%clean
[[ -d /var/lib/jenkins/rpmbuild/ ]] && cp /var/lib/jenkins/rpmbuild/RPMS/noarch/* $WORKSPACE/
rm -rf %{buildroot}

%pre

%post

%preun

%postun

%changelog


%define _prefix /opt/pentaho/biserver-ce

Name:		pentaho-biserver
Version:	%{ver}
Release:	%{rel}%{?dist}
Summary:	Pentaho BI server
License:	GPL
URL:		http://www.pentaho.com
Source:		http://downloads.sourceforge.net/project/pentaho/Business%20Intelligence%20Server/5.3/biserver-ce-6.0.1.0-386.zip
Source1:	http://meteorite.bi/downloads/saiku-plugin-p6-3.7.zip
Source2:	pentaho.init
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
Requires:	/usr/sbin/groupadd /usr/sbin/useradd /usr/sbin/groupdel /usr/sbin/userdel
Requires: 	java >= 1:1.7.0
BuildArch:	noarch

%description
%{summary}

%prep
%setup -q -T -b 1 -n saiku
%setup -q -n biserver-ce
cd %{_builddir}
mv saiku biserver-ce/pentaho-solutions/system/
cd %{_builddir}

%build

%install
rm -rf $RPM_BUILD_ROOT
%__install -d "%{buildroot}%{_prefix}"
%__install -D -m0755 "%{SOURCE2}" "%{buildroot}/etc/init.d/pentaho"
pwd
%__install -D -m0755 "start-pentaho.sh" "%{buildroot}%{_prefix}/start-pentaho.sh"
%__install -D -m0755 "stop-pentaho.sh" "%{buildroot}%{_prefix}/stop-pentaho.sh"
%__install -D -m0755 "set-pentaho-env.sh" "%{buildroot}%{_prefix}/set-pentaho-env.sh"
%__install -D -m0755 "import-export.sh" "%{buildroot}%{_prefix}/import-export.sh"
%__install -D -m0755 "promptuser.sh" "%{buildroot}%{_prefix}/promptuser.sh"

cp -pr tomcat "%{buildroot}%{_prefix}"
cp -pr pentaho-solutions "%{buildroot}%{_prefix}"
cp -pr data "%{buildroot}%{_prefix}"

%clean
rm -rf $RPM_BUILD_ROOT



%files
%defattr(-,pentaho,pentaho,-)
%attr(0755,pentaho,pentaho) %dir %{_prefix}
%doc
%{_prefix}/start-pentaho.sh
%{_prefix}/stop-pentaho.sh
%{_prefix}/set-pentaho-env.sh
%{_prefix}/import-export.sh
%{_prefix}/promptuser.sh
%{_prefix}/tomcat
%{_prefix}/pentaho-solutions
%{_prefix}/data
/etc/init.d/pentaho
%exclude %{_prefix}/tomcat/bin/*.bat
%exclude %{_prefix}/tomcat/bin/*.exe
%exclude %{_prefix}/tomcat/bin/*.tar.gz
%exclude %{_prefix}/tomcat/bin/*.dll

%pre
/usr/sbin/groupadd -r pentaho &>/dev/null || :
/usr/sbin/useradd -g pentaho -s /bin/bash -r -c "pentaho bi" \
-m -d /home/pentaho pentaho &>/dev/null || :

%post
/sbin/chkconfig --add pentaho

%preun
if [ "$1" = "0" ] ; then
    # if this is uninstallation as opposed to upgrade, delete the service
    /sbin/service pentaho stop > /dev/null 2>&1
    /sbin/chkconfig --del pentaho
fi
exit 0

%postun
if [ "$1" = "0" ]; then
  /usr/sbin/userdel pentaho
  /usr/sbin/groupdel pentaho
fi
if [ "$1" -ge 1 ]; then
    /sbin/service pentaho start > /dev/null 2>&1
fi
exit 0
